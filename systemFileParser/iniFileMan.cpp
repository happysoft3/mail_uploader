
#include "iniFileMan.h"
#include "iniParser.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IniFileMan::IniFileMan()
	: FileStream()
{
	m_parser = std::make_unique<IniParser>();
	m_parseGetFunction = &IniParser::GetData;
	m_parseSetFunction = &IniParser::SetData;
}

IniFileMan::~IniFileMan()
{ }

void IniFileMan::catchException(const std::exception &except)
{
	std::cout << stringFormat("%s%s", __FILE__, except.what()) << std::endl;
}

void IniFileMan::iniReadImpl()
{
	uint32_t iFilesize = static_cast<uint32_t>(Filesize());

	std::vector<uint8_t> stream(iFilesize);

	if (!ReadVector(stream))
		throw std::logic_error(stringFormat("%s%s: read fail", __FILE__, __FUNCDNAME__));

	m_parser->LoadData(stream);
}

bool IniFileMan::ReadIni(const std::string &inifile)
{
	bool ret = true;
	try
	{
		if (!Open(FileStreamInterface::FileOpenMode::ReadOnly, inifile))
			throw std::logic_error(stringFormat("cannot open '%s' file", inifile));

		iniReadImpl();
	}
	catch (const std::logic_error &except)
	{
		catchException(except);
		ret = false;
	}
	Close();
	return ret;
}

bool IniFileMan::writeIniPrivate(const std::string &wrStream)
{
	if (Write(reinterpret_cast<const uint8_t *>(wrStream.c_str()), wrStream.length()))
		return WriteVector({ '\n' });

	return false;
}

bool IniFileMan::WriteIni(const std::string &inifile)
{
	if (!Open(FileStreamInterface::FileOpenMode::WriteOnly, inifile))
		return false;

	m_parser->SectionPreorder([this](const std::string &cs) { return this->writeIniPrivate(cs); });

	Close();
	return true;
}

