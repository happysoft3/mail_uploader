
#ifndef LOG_LIST_VIEW_H__
#define LOG_LIST_VIEW_H__

#include "uiset/versatileListViewer.h"

class PopupDialog;

class LogListView : public VersatileListViewer
{
	DECLARE_DYNAMIC(LogListView)
private:
	std::unique_ptr<PopupDialog> m_popup;

public:
	LogListView();
	~LogListView() override;

private:
	bool selectAtEnd() const override
	{
		return true;
	}
	void changeToMystyle();
	void PreSubclassWindow() override;
	void onLogViewDoubleClicked(int line, int row);

protected:
	DECLARE_MESSAGE_MAP()
	void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
};

#endif

