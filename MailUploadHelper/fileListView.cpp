
#include "pch.h"
#include "fileListView.h"
#include "MailUploadLib/fileStatusResult.h"
#include "uiset/versatileListColumn.h"
#include "uiset/versatileListColumnItem.h"
#include "uiset/fileStatusItemPack.h"
#include "uiset/fileStatusItem.h"
#include "common/utils/myDebug.h"

IMPLEMENT_DYNAMIC(FileListView, VersatileListViewer)

FileListView::FileListView()
	: VersatileListViewer()
{ }

FileListView::~FileListView()
{ }

void FileListView::PreSubclassWindow()
{
	VersatileListViewer::PreSubclassWindow();

	auto columnData = std::make_unique<VersatileListColumn>();

	columnData->Append("filename", 550);
	columnData->Append("size", 200);
	columnData->Append("status", 200);
	PutColumn(std::move(columnData));
}

void FileListView::UpdateFileStatus(std::unique_ptr<FileStatusResult> stateResult)
{
	///All update
	for (auto &weakState : m_states)
	{
		auto state = weakState.lock();

		if (state)
			state->m_state = stateResult->GetStatus(state->m_index);
	}
	Invalidate();
}

void FileListView::PutFileList(std::list<std::unique_ptr<VersatileListModel>> &files)
{
	if (files.empty())
		return;

	size_t boundary = files.size();

	m_states.resize(files.size());
	std::list<std::unique_ptr<VersatileListModel>> inItem;
	for (;;)
	{
		if (files.empty())
			break;

		std::unique_ptr<VersatileListModel> pack = std::move(files.front());
		FileStatusItemPack *pValid = dynamic_cast<FileStatusItemPack *>(pack.get());

		if (!pValid)
			MY_THROW() << "bad cast";

		files.pop_front();
		auto status = pValid->GetStatus();
		size_t index = static_cast<size_t>(status->m_index);

		if (index >= boundary)
			MY_THROW() << "expect max " << files.size() << ", but got " << index;

		m_states[index] = status;
		inItem.push_back(std::move(pack));
	}
	Append(inItem);
}

BEGIN_MESSAGE_MAP(FileListView, VersatileListViewer)
	ON_WM_NCCALCSIZE()
END_MESSAGE_MAP()

void FileListView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	ModifyStyle(WS_HSCROLL, 0);

	CListCtrl::OnNcCalcSize(bCalcValidRects, lpncsp);
}