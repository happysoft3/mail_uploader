
#ifndef LOAD_PARAM_THREAD_H__
#define LOAD_PARAM_THREAD_H__

#include "common/task/taskThread.h"

class LoadParamThread : public TaskThread
{
public:
	LoadParamThread();
	~LoadParamThread() override;

private:
	void afterExec(TaskResultHash &resultMap) noexcept override;

	DECLARE_SIGNAL(OnFinished)
};

#endif

