
#ifndef LOAD_PARAM_TASK_H__
#define LOAD_PARAM_TASK_H__

#include "common/task/taskUnit.h"

class ParameterLoader;
class LoadParamResult;
class VersatileListElement;

class LoadParamTask : public TaskUnit
{
private:
	std::string m_cmd;
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::unique_ptr<LoadParamResult> m_result;

public:
	LoadParamTask();
	~LoadParamTask() override;

private:
	void loadFileDataImpl(const std::string &url, int index);
	void loadFileData();
	void Run() override;
	std::unique_ptr<TaskResult> GetResult() override;

public:
	bool PutCommandLine(const std::string &cmd);
};

#endif

