
#ifndef LOAD_PARAM_RESULT_H__
#define LOAD_PARAM_RESULT_H__

#include "common/task/taskResult.h"

class ParameterLoader;
class VersatileListModel;

class LoadParamResult : public TaskResult
{
private:
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::list<std::unique_ptr<VersatileListModel>> m_colData;
	int m_loadedCount;

public:
	LoadParamResult();
	~LoadParamResult();

	int FileCount() const
	{
		return m_loadedCount;		
	}
	void Push(std::unique_ptr<VersatileListModel> elem);
	void PutParamLoader(std::unique_ptr<ParameterLoader> loader);
	std::unique_ptr<ParameterLoader> DetachParamLoader();
	void StealColumnData(std::list<std::unique_ptr<VersatileListModel>> &steal);
};

#endif
