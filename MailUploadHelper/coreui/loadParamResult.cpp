
#include "pch.h"
#include "loadParamResult.h"
#include "uiset/versatileListModel.h"
#include "MailUploadLib/parameterLoader.h"

LoadParamResult::LoadParamResult()
	: TaskResult()
{
	m_loadedCount = 0;
}

LoadParamResult::~LoadParamResult()
{ }

void LoadParamResult::Push(std::unique_ptr<VersatileListModel> elem)
{
	m_colData.push_back(std::move(elem));
}

void LoadParamResult::PutParamLoader(std::unique_ptr<ParameterLoader> loader)
{
	m_loadedCount = loader->LoadedCount();
	m_paramLoader = std::move(loader);
}

std::unique_ptr<ParameterLoader> LoadParamResult::DetachParamLoader()
{
	return m_paramLoader ? std::move(m_paramLoader) : nullptr;
}

void LoadParamResult::StealColumnData(std::list<std::unique_ptr<VersatileListModel>> &steal)
{
	for (auto &elem : m_colData)
		steal.push_back(std::move(elem));
	m_colData.clear();
}

