
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include "common/ccobject/ccobject.h"
#include <memory>
#include <string>

class ParameterLoader;
class UploadManager;
class LoadParamThread;
class TaskResult;
class LoadParamResult;
class LogQueue;
class PercentGage;
class FileStatusResult;

class CoreUi : public CCObject
{
private:
	CWnd *m_ownerWindow;
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::unique_ptr<std::exception> m_error;
	std::unique_ptr<LoadParamThread> m_loadParamThread;
	std::unique_ptr<UploadManager> m_uploadMan;
	std::shared_ptr<LogQueue> m_logQueue;
	std::unique_ptr<PercentGage> m_gage;

public:
	CoreUi(CWnd *ownerWindow = nullptr);
	~CoreUi() override;

private:
	void slotLoadedParam();

public:
	void TryLoadParam(const std::string &cmd);
	std::unique_ptr<TaskResult> FetchParam();

private:
	void uploadImpl();
	void slotPrintLog(const std::string &msg);
	void slotUpdateProgress(int prog);
	void slotFinishUpload();

public:
	void PutLogQueue(std::shared_ptr<LogQueue> &logQueue)
	{
		m_logQueue = logQueue;
	}
	bool DoUpload();
	void DoAbort();
	std::unique_ptr<std::exception> GetError();
	std::unique_ptr<FileStatusResult> CompleteUpload();

public:
	static constexpr UINT DispatchMsgCoreUi = WM_USER + 1;
	struct MessageId
	{
		enum Message
		{
			NONE,
			LoadedParam,
			PushLogMessage,
			EndUpload,
			ReportProgress,
		};
	};
};

#endif

