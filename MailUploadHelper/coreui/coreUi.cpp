
#include "pch.h"
#include "coreUi.h"
#include "loadParamThread.h"
#include "common/task/taskResultHash.h"
#include "common/task/taskQueue.h"
#include "loadParamTask.h"
#include "loadParamResult.h"
#include "MailUploadLib/parameterLoader.h"
#include "MailUploadLib/uploadManager.h"
#include "mailuploadlib/fileStatusResult.h"
#include "common/utils/pathManager.h"
#include "logQueue.h"
#include "uiset/versatileListModel.h"
#include "common/include/incFilesystem.h"
#include "common/utils/myDebug.h"
#include <algorithm>

class PercentGage
{
private:
	const int m_max;
	int m_prev;
	const uint32_t m_gap;
	int m_computed;

public:
	PercentGage(int max, uint32_t gap = 1)
		: m_max(max), m_gap(gap == 0 ? 1 : gap)
	{
		m_prev = 0;
	}
	bool Compute(int value)
	{
		m_computed = (value * 100) / m_max;
		bool ret = m_computed - m_prev >= static_cast<int>(m_gap);

		if (ret)
			m_prev = m_computed;
		return ret;
	}
	bool ReachedAtMax(int value) const
	{
		return value == m_max;
	}
};

CoreUi::CoreUi(CWnd *ownerWindow)
	: CCObject()
{
	m_ownerWindow = ownerWindow;
}

CoreUi::~CoreUi()
{ }

void CoreUi::slotLoadedParam()
{
	if (m_ownerWindow)
		m_ownerWindow->PostMessageA(DispatchMsgCoreUi, MessageId::LoadedParam);
}

void CoreUi::TryLoadParam(const std::string &cmd)
{
	auto task = std::make_unique<LoadParamTask>();

	if (!task->PutCommandLine(cmd))
	{
		auto error = task->GetError();

		slotPrintLog(error->what());
		return;
	}

	auto taskQueue = std::make_shared<TaskQueue>();

	taskQueue->Push(std::move(task));
	m_loadParamThread = std::make_unique<LoadParamThread>();

	m_loadParamThread->OnFinished().Connection(&CoreUi::slotLoadedParam, this);
	m_loadParamThread->Perform(taskQueue);
}

std::unique_ptr<TaskResult> CoreUi::FetchParam()
{
	if (!m_loadParamThread)
		return {};

	std::unique_ptr<LoadParamThread> paramThread = std::move(m_loadParamThread);
	auto resMap = paramThread->GetResult();
	auto res = resMap->Pop();
	
	if (!res)
		MY_THROW() << "no result";

	LoadParamResult *convResult = dynamic_cast<LoadParamResult *>(res.get());

	if (!convResult)
		MY_THROW() << "bad cast";

	m_paramLoader = convResult->DetachParamLoader();
	auto err = res->GetError();

	m_gage = std::make_unique<PercentGage>(m_paramLoader->LoadedCount(), 3);

	if (err)
		MY_THROW() << err->what();

	return res;
}

void CoreUi::uploadImpl()
{
	if (!m_paramLoader)
		throw std::logic_error("param isn't loaded");

	m_uploadMan->PrematurePreparation(std::move(m_paramLoader));
	m_uploadMan->DoUpload();
}

void CoreUi::slotPrintLog(const std::string &msg)
{
	if (m_ownerWindow)
	{
		m_logQueue->MakePush(msg);
		m_ownerWindow->PostMessageA(DispatchMsgCoreUi, MessageId::PushLogMessage);
	}
}

void CoreUi::slotUpdateProgress(int prog)
{
	if (!m_ownerWindow)
		return;

	if (m_gage->Compute(prog) || m_gage->ReachedAtMax(prog))
		m_ownerWindow->PostMessageA(DispatchMsgCoreUi, MessageId::ReportProgress, prog);
}

void CoreUi::slotFinishUpload()
{
	if (m_ownerWindow)
		m_ownerWindow->PostMessageA(DispatchMsgCoreUi, MessageId::EndUpload);
}

bool CoreUi::DoUpload()
{
	if (m_uploadMan)
		return false;
	if (!m_paramLoader)
		return false;

	m_uploadMan = std::make_unique<UploadManager>();
	m_uploadMan->OnPrintLog().Connection(&CoreUi::slotPrintLog, this);
	m_uploadMan->OnReportProgress().Connection(&CoreUi::slotUpdateProgress, this);
	m_uploadMan->OnReportEndProgress().Connection(&CoreUi::slotFinishUpload, this);

	try
	{
		uploadImpl();
	}
	catch (const std::exception &except)
	{
		m_error = std::make_unique<std::exception>(except);
		m_uploadMan.reset();
		return false;
	}
	return true;
}

void CoreUi::DoAbort()
{
	if (m_uploadMan)
		m_uploadMan->CancelUpload();
}

std::unique_ptr<std::exception> CoreUi::GetError()
{
	return m_error ? std::move(m_error) : nullptr;
}

std::unique_ptr<FileStatusResult> CoreUi::CompleteUpload()
{
	if (!m_uploadMan)
		return {};

	return m_uploadMan->CheckResult();
#if 0
	auto ret = m_uploadMan->CheckResult();

	m_uploadMan.reset();
	return ret;
#endif
}


