
#include "pch.h"
#include "loadParamThread.h"
#include "common/task/taskResultHash.h"

LoadParamThread::LoadParamThread()
	: TaskThread()
{ }

LoadParamThread::~LoadParamThread()
{ }

void LoadParamThread::afterExec(TaskResultHash &resultMap) noexcept
{
	m_OnFinished.Emit();
}

