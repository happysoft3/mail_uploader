
#include "pch.h"
#include "loadParamTask.h"
#include "loadParamResult.h"
#include "MailUploadLib/parameterLoader.h"
//#include "uiset/versatileListElement.h"
#include "uiset/fileStatusItemPack.h"
#include "uiset/fileStatusItem.h"
#include "common/utils/pathManager.h"
#include "common/utils/myDebug.h"
#include "common/include/incFilesystem.h"

LoadParamTask::LoadParamTask()
	: TaskUnit()
{ }

LoadParamTask::~LoadParamTask()
{ }

void LoadParamTask::loadFileDataImpl(const std::string &url, int index)
{
	auto pathMan = m_paramLoader->CreatePath(url);
	auto pth = pathMan->FullPath();
	size_t fileSize = 0;

	try
	{
		fileSize = static_cast<size_t>(NAMESPACE_FILESYSTEM::file_size(pth));
	}
	catch (std::exception &ex)
	{
		MY_PRINT() << ex.what();
	}

	auto elem = std::make_unique<FileStatusItemPack>();

	elem->PutFileItem(url, fileSize, index);
	m_result->Push(std::move(elem));
}

void LoadParamTask::loadFileData()
{
	if (!m_paramLoader)
		MY_THROW() << "no param";

	m_paramLoader->PreorderFileList([this](const std::string &n, int index)
	{
		loadFileDataImpl(n, index);
	}, false);
}

void LoadParamTask::Run()
{
	m_result = std::make_unique<LoadParamResult>();

	try
	{
		loadFileData();
	}
	catch (std::exception &ex)
	{
		m_result->PutError(ex);
	}
	if (m_paramLoader)
		m_result->PutParamLoader(std::move(m_paramLoader));
}

std::unique_ptr<TaskResult> LoadParamTask::GetResult()
{
	return m_result ? std::move(m_result) : nullptr;
}

bool LoadParamTask::PutCommandLine(const std::string &cmd)
{
	std::string iniPath;
	auto paramLoader = std::make_unique<ParameterLoader>();

	iniPath.reserve(cmd.size());
	for (const auto &c : cmd)
	{
		switch (c)
		{
		case '/': iniPath.push_back('\\'); break;
		case '"': break;
		default: iniPath.push_back(c); break;
		}
	}

	try
	{
		
		if (!paramLoader->ReadIni(iniPath))
			throw std::logic_error("open failure");
	}
	catch (std::exception &except)
	{
		PutError(except);
		return false;
	}
	m_paramLoader = std::move(paramLoader);
	return true;
}

