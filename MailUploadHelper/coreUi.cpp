
#include "pch.h"
#include "coreUi.h"
#include "MailUploadLib/parameterLoader.h"
#include "MailUploadLib/uploadManager.h"
#include <algorithm>

CoreUi::CoreUi()
{ }

CoreUi::~CoreUi()
{ }

std::unique_ptr<ParameterLoader> CoreUi::TranslateCommandLine(const std::string &cmd)
{
	std::string iniPath = cmd;
	auto paramLoader = std::make_unique<ParameterLoader>();

	std::transform(iniPath.cbegin(), iniPath.cend(), iniPath.begin(), [](const auto &c) { return c == '/' ? '\\' : c; });
	try
	{
		if (!paramLoader->ReadIni(cmd))
			throw std::logic_error("open failure");
	}
	catch (const std::exception &except)
	{
		m_error = std::make_unique<std::exception>(except);
		return {};
	}
	return paramLoader;
}

void CoreUi::uploadImpl()
{
	if (!m_paramLoader)
		throw std::logic_error("param isn't loaded");

	auto upman = std::make_unique<UploadManager>();

	upman->PrematurePreparation(std::move(m_paramLoader));
}

bool CoreUi::DoUpload()
{
	try
	{
		uploadImpl();
	}
	catch (const std::exception &except)
	{
		m_error = std::make_unique<std::exception>(except);
		return false;
	}
	return true;
}

std::unique_ptr<std::exception> CoreUi::GetError()
{
	return m_error ? std::move(m_error) : nullptr;
}



