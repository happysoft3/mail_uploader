﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// MailUploadHelper.rc에서 사용되고 있습니다.
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MAILUPLOADHELPER_DIALOG     102
#define IDR_MAINFRAME                   128
#define IDD_POPUP_DIALOG                130
#define IDB_BITMAP1                     132
#define IDC_UPLOAD_BUTTON               1000
#define IDC_UPLOAD_PROGRESS             1001
#define IDC_UPLOAD_LIST                 1002
#define IDC_UPLOAD_GO                   1003
#define IDC_UPLOAD_LOG                  1004
#define IDC_MAIN_FRAME                  1006
#define IDC_POPUP_CLOSE                 1007
#define IDC_POPUP_TEXT                  1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
