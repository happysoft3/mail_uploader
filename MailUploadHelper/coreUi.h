
#ifndef CORE_UI_H__
#define CORE_UI_H__

#include <memory>
#include <string>

class ParameterLoader;
class UploadManager;

class CoreUi
{
private:
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::unique_ptr<std::exception> m_error;

public:
	CoreUi();
	~CoreUi();

public:
	std::unique_ptr<ParameterLoader> TranslateCommandLine(const std::string &cmd);

private:
	void uploadImpl();

public:
	bool DoUpload();
	std::unique_ptr<std::exception> GetError();
};

#endif

