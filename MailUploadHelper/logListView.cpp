
#include "pch.h"
#include "logListView.h"
#include "uiset/versatileListColumn.h"
#include "uiset/versatileListColumnItem.h"
#include "dialog/popupDialog.h"

IMPLEMENT_DYNAMIC(LogListView, VersatileListViewer)

LogListView::LogListView()
	: VersatileListViewer()
{ }

LogListView::~LogListView()
{ }

void LogListView::PreSubclassWindow()
{
	VersatileListViewer::PreSubclassWindow();
	CRect area;

	GetClientRect(area);

	auto columnData = std::make_unique<VersatileListColumn>();
	int width = area.Width();

	constexpr int firstWidth = 500;

	columnData->Append("message", firstWidth);
	columnData->Append("time", width - firstWidth );
	PutColumn(std::move(columnData));
	SetDoubleClickCallback([this](int l, int r)
	{
		onLogViewDoubleClicked(l, r);
	});

	m_popup = std::make_unique<PopupDialog>(this);
}

void LogListView::onLogViewDoubleClicked(int line, int row)
{
	if (row == 0)
	{
		CString msg = GetItemText(line, row);

		m_popup->SetBodyText(msg.GetString());
		m_popup->Show();
	}
}

BEGIN_MESSAGE_MAP(LogListView, VersatileListViewer)
	ON_WM_NCCALCSIZE()
END_MESSAGE_MAP()

void LogListView::OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp)
{
	ModifyStyle(WS_HSCROLL, 0);

	CListCtrl::OnNcCalcSize(bCalcValidRects, lpncsp);
}




