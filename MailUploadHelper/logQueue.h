
#ifndef LOG_QUEUE_H__
#define LOG_QUEUE_H__

#include <list>
#include <mutex>
#include <memory>
#include <string>

class VersatileListModel;
class VersatileListItem;

class LogQueue
{
private:
	std::list<std::unique_ptr<VersatileListModel>> m_logs;

public:
	LogQueue();
	~LogQueue();

private:
	void writeTime(VersatileListItem &logItem);

public:
	void Push(std::unique_ptr<VersatileListModel> log);
	void MakePush(const std::string &msg);
	void Steal(std::list<std::unique_ptr<VersatileListModel>> &steal);

private:
	std::mutex m_lock;
};

#endif
