
#include "pch.h"
#include "fileStatusItemPack.h"
#include "fileStatusItem.h"

FileStatusItemPack::FileStatusItemPack()
	: VersatileListModel()
{ }

FileStatusItemPack::~FileStatusItemPack()
{ }

void FileStatusItemPack::PutFileItem(const std::string &url, size_t fileSize, int index)
{
	auto fsItem = std::make_unique<FileStatusItem>(index);

	m_statusData = fsItem->GetStatus();
	Emplace<VersatileListItem>(url);
	Emplace<VersatileListItem>(std::to_string(fileSize));
	Push(std::move(fsItem));
}

std::shared_ptr<FileStatusData> FileStatusItemPack::GetStatus()
{
	/*std::shared_ptr<FileStatusData> state = std::move(m_statusData);

	return state;*/
	return m_statusData;
}

uint32_t FileStatusItemPack::TextColor() const
{
	return m_statusData->m_state == "NG" ? RGB(255, 0, 0) : VersatileListModel::TextColor();
}

