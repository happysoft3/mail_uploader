
#ifndef FILE_STATUS_ITEM_H__
#define FILE_STATUS_ITEM_H__

#include "versatileListItem.h"

struct FileStatusData
{
	std::string m_state;
	int m_index;
	//+error object
};

class FileStatusItem : public VersatileListItem
{
private:
	std::shared_ptr<FileStatusData> m_statusData;

public:
	FileStatusItem(int index);
	~FileStatusItem() override;

private:
	void ListviewInfoText(LV_ITEM *pItem) override;

public:
	int Index() const
	{
		return m_statusData->m_index;
	}
	std::shared_ptr<FileStatusData> GetStatus() const
	{
		return m_statusData;
	}
};

#endif

