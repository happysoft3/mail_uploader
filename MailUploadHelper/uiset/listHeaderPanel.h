
#ifndef LIST_HEADER_PANEL_H__
#define LIST_HEADER_PANEL_H__

#include <memory>

class VersatileListColumn;
class VersatileListColumnItem;

class ListHeaderPanel : public CHeaderCtrl
{
	DECLARE_DYNAMIC(ListHeaderPanel)

private:
	std::unique_ptr<VersatileListColumn> m_columnData;
	int m_colCount;
	int m_curIndex;
	int m_validWidth;

public:
	ListHeaderPanel();
	~ListHeaderPanel() override;
	
private:
	void drawImpl(CDC &cdc, VersatileListColumnItem &item);

public:
	void Draw(CDC &cdc);
	void AttachColumn(std::unique_ptr<VersatileListColumn> columnData);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
};

#endif
