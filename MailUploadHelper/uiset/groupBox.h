
#ifndef GROUP_BOX_H__
#define GROUP_BOX_H__

#include <list>

class GroupBox : public CStatic
{
	DECLARE_DYNAMIC(GroupBox)

private:
	CBrush m_brush;
	std::list<CWnd *> m_overlappedWnd;

public:
	explicit GroupBox();
	~GroupBox() override;

private:
	virtual void PreSubclassWindow() override;
	void drawStuff(CDC &cdc);
	void drawWindow(CDC &cdc);

public:
	void PutOverayWindow(CWnd *pWnd);

protected:
	afx_msg void OnPaint();
	BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg HBRUSH CtlColor(CDC *pDC, UINT nCtlColor);
	DECLARE_MESSAGE_MAP()
};

#endif
