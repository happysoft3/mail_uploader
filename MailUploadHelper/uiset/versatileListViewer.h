
#ifndef LOG_LIST_VIEWER_H__
#define LOG_LIST_VIEWER_H__

#include <list>
#include <tuple>
#include <memory>
#include <vector>
#include <mutex>
#include <functional>

class ListHeaderPanel;
class VersatileListColumn;
class VersatileListModel;

class VersatileListViewer : public CListCtrl
{
	DECLARE_DYNAMIC(VersatileListViewer)

private:
	using list_element_ty = std::unique_ptr<VersatileListModel>;
	std::unique_ptr<ListHeaderPanel> m_headerPanel;
	std::vector<list_element_ty> m_elementVec;
	std::unique_ptr<VersatileListModel> m_element;
	int m_selectedColumn;
	std::function<void(int, int)> m_onDbClick;

public:
	VersatileListViewer();
	~VersatileListViewer() override;

public:
	void PutColumn(std::unique_ptr<VersatileListColumn> columnData);
	void SetDoubleClickCallback(std::function<void(int, int)> onDbClick);

private:
	bool pullElement(const size_t inputSize);
	virtual bool selectAtEnd() const;
	void updateListItem(const size_t count, bool isFull);

public:
	void AppendSingle(list_element_ty &elem);
	void Append(std::list<list_element_ty> &addList);

private:
	void EnableHighlighting(HWND hWnd, int row, bool bHighlight);
	bool IsRowSelected(HWND hWnd, int row);
	bool IsRowHighlighted(HWND hWnd, int row);

private:
	void fetchListPanelArea(CDC &cdc, CRect &area);
	void getHeaderPanelArea(CRect &area);
	void drawListCell(CDC &cdc, const CRect &area);
	void drawList(CDC &cdc);
	void drawBackground(CDC &cdc);

protected:
	void PreSubclassWindow() override;
	DECLARE_MESSAGE_MAP()
	afx_msg BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT *pResult);
	afx_msg void OnGetDisplayInfoList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDbClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnPaint();

private:
	std::recursive_mutex m_lock;
};

#endif

