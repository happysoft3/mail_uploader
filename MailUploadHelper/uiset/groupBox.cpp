
#include "pch.h"
#include "groupbox.h"
#include "cbufferdc.h"

IMPLEMENT_DYNAMIC(GroupBox, CStatic)

GroupBox::GroupBox()
	: CStatic(), m_brush(RGB(250, 250, 250))
{
}

GroupBox::~GroupBox()
{
}

void GroupBox::PreSubclassWindow()
{
	SetWindowText("");
}

void GroupBox::drawStuff(CDC &cdc)
{
	CPen pen(PS_SOLID, 2, RGB(106, 182, 196));
	CPen *oldpen = cdc.SelectObject(&pen);
	CRect rect;

	cdc.SetBkMode(TRANSPARENT);

	GetClientRect(&rect);
	cdc.Rectangle(&rect);

	CBrush brush;
	CRect clip;

	cdc.GetClipBox(clip);
	brush.CreateSolidBrush(RGB(112, 146, 190));
	cdc.FillRect(clip, &brush);
	cdc.SelectObject(oldpen);
}

void GroupBox::drawWindow(CDC &cdc)
{
	CRect validArea;

	for (auto &pwnd : m_overlappedWnd)
	{
		pwnd->GetWindowRect(validArea);
		ScreenToClient(validArea);
		cdc.ExcludeClipRect(validArea);
	}
	drawStuff(cdc);
}

void GroupBox::PutOverayWindow(CWnd *pWnd)
{
	m_overlappedWnd.push_back(pWnd);
}

BEGIN_MESSAGE_MAP(GroupBox, CStatic)
	ON_WM_PAINT()
	//ON_WM_CTLCOLOR_REFLECT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

void GroupBox::OnPaint()
{
	CBufferDC cdc(this);

	drawWindow(cdc);
}

BOOL GroupBox::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}

HBRUSH GroupBox::CtlColor(CDC *pDC, UINT nCtlColor)
{
	pDC->SetBkColor(RGB(251, 248, 239));

	return m_brush;
}
