
#ifndef VERSATILE_LIST_COLUMN_H__
#define VERSATILE_LIST_COLUMN_H__

#include "abstractListModel.h"
#include <functional>

class VersatileListColumnItem;

class VersatileListColumn : public AbstractListModel
{
private:
	int m_offset;
	int m_columns;

public:
	VersatileListColumn();
	~VersatileListColumn() override;

public:
	void Append(const std::string &name, int width);

private:
	void insertElementItem(VersatileListColumnItem &item, CListCtrl &viewer, const CRect &area);

public:
	void Fetch(CListCtrl &viewer);
};

#endif
