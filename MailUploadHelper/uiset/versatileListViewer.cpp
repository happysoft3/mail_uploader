
#include "pch.h"
#include "versatileListViewer.h"
#include "listheaderpanel.h"
#include "uiset/versatileListColumn.h"
#include "uiset/versatileListModel.h"
#include "uiset/versatileListItem.h"
#include "cbufferdc.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IMPLEMENT_DYNAMIC(VersatileListViewer, CListCtrl)

static constexpr UINT s_list_base_color = RGB(250, 250, 250);
static constexpr UINT s_list_border_color = RGB(220, 218, 218);

VersatileListViewer::VersatileListViewer()
	: CListCtrl()
{
	m_headerPanel = std::make_unique<ListHeaderPanel>();
	m_elementVec.reserve(65536);
	m_selectedColumn = -1;
}

VersatileListViewer::~VersatileListViewer()
{ }

void VersatileListViewer::PutColumn(std::unique_ptr<VersatileListColumn> column)
{
	column->Fetch(*this);
	m_headerPanel->AttachColumn(std::move(column));
}

void VersatileListViewer::SetDoubleClickCallback(std::function<void(int, int)> onDbClick)
{
	m_onDbClick = onDbClick;
}

bool VersatileListViewer::pullElement(const size_t inputSize)
{
	std::unique_lock<std::recursive_mutex> locker(m_lock);

	const size_t totalSize = m_elementVec.size() + inputSize;

	if (totalSize > m_elementVec.capacity())
	{
		const size_t pullCount = totalSize - m_elementVec.capacity();

		if (!pullCount)
			return false;

		auto posIter = m_elementVec.begin() + pullCount;

		for (auto &elem : m_elementVec)
		{
			if (posIter == m_elementVec.end())
			{
				m_elementVec.resize(m_elementVec.size() - pullCount);
				return true;
			}
			elem = std::move(*posIter);
			++posIter;
		}
	}
	return false;
}

bool VersatileListViewer::selectAtEnd() const
{
	return (m_selectedColumn >= 0) ? (GetItemCount() - 1 == m_selectedColumn) : false;
}

void VersatileListViewer::updateListItem(const size_t count, bool isFull)
{
	if (!isFull)
	{
		bool selEnd = selectAtEnd();

		SetItemCountEx(count, LVSICF_NOINVALIDATEALL | LVSICF_NOSCROLL);
		if (selEnd)
		{
			SetItemState(GetItemCount() - 1, LVNI_SELECTED | LVNI_FOCUSED, 0xf);
			EnsureVisible(GetItemCount() - 1, false);
		}
	}
	else
	{
		int topIndex = GetTopIndex();
		size_t last = topIndex + GetCountPerPage();

		SetRedraw(false);
		while (last--)
		{
			if (last < count)
				Update(last);
		}
		SetRedraw(true);
	}
}

void VersatileListViewer::AppendSingle(VersatileListViewer::list_element_ty &elem)
{
	std::list<list_element_ty> param;

	param.push_back(std::move(elem));
	Append(param);
}

void VersatileListViewer::Append(std::list<VersatileListViewer::list_element_ty> &addList)
{
	bool isFull = pullElement(addList.size());
	size_t elemCount = 0;

	{
		std::unique_lock<std::recursive_mutex> locker(m_lock);

		for (auto &&element : addList)
			m_elementVec.push_back(std::move(element));

		elemCount = m_elementVec.size();
	}
	updateListItem(elemCount, isFull);
}

void VersatileListViewer::EnableHighlighting(HWND hWnd, int row, bool bHighlight)
{
	ListView_SetItemState(hWnd, row, bHighlight ? 0xff : 0, LVIS_SELECTED);
}

bool VersatileListViewer::IsRowSelected(HWND hWnd, int row)
{
	return ListView_GetItemState(hWnd, row, LVIS_SELECTED) != 0;
}

bool VersatileListViewer::IsRowHighlighted(HWND hWnd, int row)
{
	return IsRowSelected(hWnd, row);
}

void VersatileListViewer::fetchListPanelArea(CDC &cdc, CRect &area)
{
	auto *pHeaderCtrl = GetHeaderCtrl();

	if (pHeaderCtrl)
	{
		CRect headerArea;

		pHeaderCtrl->GetWindowRect(headerArea);
		ScreenToClient(headerArea);
		cdc.ExcludeClipRect(headerArea);
	}
	cdc.GetClipBox(area);
}

void VersatileListViewer::getHeaderPanelArea(CRect &area)
{
	m_headerPanel->GetWindowRect(area);
	ScreenToClient(area);
}

void VersatileListViewer::drawListCell(CDC &cdc, const CRect &area)
{
	int itemCount = GetItemCount();

	if (itemCount > 0)
	{
		CRect fieldArea;
		CPoint point;
		CRect itemArea;

		GetItemRect(itemCount - 1, itemArea, LVIR_BOUNDS);
		GetItemPosition(itemCount - 1, &point);
		fieldArea.top = area.top;
		fieldArea.left = point.x;
		fieldArea.right = itemArea.right - 3;
		fieldArea.bottom = itemArea.bottom;

		DefWindowProc(WM_PAINT, (WPARAM)cdc.m_hDC, (LPARAM)0);
		cdc.ExcludeClipRect(fieldArea);
	}
}

void VersatileListViewer::drawList(CDC &cdc)
{
	CRect area;

	getHeaderPanelArea(area);
	m_headerPanel->Draw(cdc);
	cdc.ExcludeClipRect(area);
	drawListCell(cdc, area);
}

void VersatileListViewer::drawBackground(CDC &cdc)
{
	//CBrush brush;
	//CRect clip;

	//cdc.GetClipBox(clip);
	//brush.CreateSolidBrush(s_list_base_color);
	//cdc.FillRect(clip, &brush);
	CPen pen(PS_SOLID, 2, s_list_border_color);
	CBrush brush;
	CPen *oldPen = cdc.SelectObject(&pen);
	CRect clip;
	CBrush *oldBrush = cdc.SelectObject(&brush);

	cdc.GetClipBox(clip);
	brush.CreateSolidBrush(s_list_base_color);
	cdc.Rectangle(clip);
	cdc.SelectObject(oldPen);
	cdc.SelectObject(oldBrush);
}

void VersatileListViewer::PreSubclassWindow()
{
	SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	ModifyStyle(0, LVS_SINGLESEL);

	CHeaderCtrl *pHeaderCtrl = GetHeaderCtrl();
	HWND hWnd = reinterpret_cast<HWND>(::SendMessage(m_hWnd, LVM_GETHEADER, 0, 0));

	pHeaderCtrl->ModifyStyle(0, LVS_OWNERDRAWFIXED, SWP_FRAMECHANGED);
	m_headerPanel->SubclassWindow(hWnd);

	CListCtrl::PreSubclassWindow();
}

BEGIN_MESSAGE_MAP(VersatileListViewer, CListCtrl)
	ON_NOTIFY_REFLECT(LVN_GETDISPINFO, OnGetDisplayInfoList)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnNMDbClick)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL VersatileListViewer::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT *pResult)
{
	switch (((NMHDR *)lParam)->code)
	{
	case HDN_BEGINTRACKW:
	case HDN_BEGINTRACKA:
		*pResult = TRUE;

		return TRUE;
	}

	return CListCtrl::OnNotify(wParam, lParam, pResult);
}

void VersatileListViewer::OnGetDisplayInfoList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO *pDispInfo = reinterpret_cast<LV_DISPINFO *>(pNMHDR);
	LV_ITEM *pItem = &(pDispInfo->item);
	int itemId = pItem->iItem;

	{
		std::unique_lock<std::recursive_mutex> locker(m_lock);
		auto elem = m_elementVec[itemId]->GetItem<VersatileListItem>(pItem->iSubItem);

		if (pItem->mask & LVIF_TEXT)
			elem->ListviewInfoText(pItem);
		if (pItem->mask & LVIF_IMAGE)
			elem->ListviewInfoImage(pItem);
	}
	*pResult = 0;
}

void VersatileListViewer::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	NMTVCUSTOMDRAW *pLVCD = reinterpret_cast<NMTVCUSTOMDRAW *>(pNMHDR);
	static bool bHighlighted = false;

	*pResult = CDRF_DODEFAULT;
	if (CDDS_PREPAINT == pNMCD->dwDrawStage)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if (CDDS_ITEMPREPAINT == pNMCD->dwDrawStage)
	{
		int iRow = static_cast<int>(pNMCD->dwItemSpec);

		bHighlighted = IsRowHighlighted(m_hWnd, iRow);

		if (bHighlighted)
		{
			pNMCD->uItemState = CDIS_DEFAULT;
			pLVCD->clrText = RGB(16, 16, 16); // Use my foreground hilite color
			pLVCD->clrTextBk = RGB(221, 233, 247); // Use my background hilite color
			if (m_selectedColumn != iRow)
				m_selectedColumn = iRow;
		}
		else
		{
			std::unique_lock<std::recursive_mutex> locker(m_lock);

			pLVCD->clrText = m_elementVec[iRow]->TextColor();
			pLVCD->clrTextBk = s_list_base_color;
		}

		*pResult = CDRF_DODEFAULT | CDRF_NOTIFYPOSTPAINT;
	}
	else if (CDDS_ITEMPOSTPAINT == pNMCD->dwDrawStage)
	{
		if (bHighlighted)
		{
			int iRow = static_cast<int>(pNMCD->dwItemSpec);

			EnableHighlighting(m_hWnd, iRow, true);
		}
		*pResult = CDRF_DODEFAULT;
	}
}

void VersatileListViewer::OnNMDbClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE lpnmItem = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int columnId = lpnmItem->iItem;
	int rowId = lpnmItem->iSubItem;

	if (columnId != -1)
	{
		if (m_onDbClick)
			m_onDbClick(columnId, rowId);
	}
}

BOOL VersatileListViewer::OnEraseBkgnd(CDC *pDC)
{
	return false;
}

void VersatileListViewer::OnPaint()
{
	CBufferDC cdc(this);

	drawList(cdc);
	drawBackground(cdc);
}

