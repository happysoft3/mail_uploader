
#include "pch.h"
#include "abstractListModel.h"
#include "abstractListItem.h"

AbstractListModel::AbstractListModel()
{ }

AbstractListModel::~AbstractListModel()
{ }

void AbstractListModel::Push(std::unique_ptr<AbstractListItem> item)
{
	m_itemVector.push_back(std::move(item));
}
