
#include "pch.h"
#include "fileStatusItem.h"

static constexpr auto empty_field = "-";

FileStatusItem::FileStatusItem(int index)
	: VersatileListItem()
{
	m_statusData = std::make_shared<FileStatusData>();
	m_statusData->m_state = empty_field;
	m_statusData->m_index = index;
}

FileStatusItem::~FileStatusItem()
{ }

void FileStatusItem::ListviewInfoText(LV_ITEM *pItem)
{
	const char *txt = empty_field;

	if (m_statusData)
	{
		if (!m_statusData->m_state.empty())
			txt = m_statusData->m_state.c_str();
	}
	lstrcpyn(pItem->pszText, txt, pItem->cchTextMax);
}

