
#include "pch.h"
#include "versatileListColumnItem.h"

static constexpr UINT header_ctrl_first_color = RGB(220, 218, 218);
static constexpr UINT header_ctrl_second_color = RGB(218, 214, 214);
static constexpr UINT header_ctrl_text_color = RGB(9, 16, 9);

VersatileListColumnItem::VersatileListColumnItem()
	: AbstractListItem()
{ }

VersatileListColumnItem::VersatileListColumnItem(const std::string &name, int width)
	: AbstractListItem()
{
	m_width = width;
	m_name = name;
}

VersatileListColumnItem::~VersatileListColumnItem()
{ }

int VersatileListColumnItem::Format() const
{
	return LVCFMT_LEFT/* | LVCFMT_FIXED_WIDTH | LVIF_TEXT*/;
}

void VersatileListColumnItem::maybeDrawBackground(CDC &cdc, const CRect &area)
{
	TRIVERTEX triver[2];

	triver[0].x = area.left;
	triver[0].y = area.top;
	triver[0].Red = GetRValue(header_ctrl_first_color) << 8;
	triver[0].Green = GetGValue(header_ctrl_first_color) << 8;
	triver[0].Blue = GetBValue(header_ctrl_first_color) << 8;
	triver[0].Alpha = 0;

	//End Gradient Info
	triver[1].x = area.right;
	triver[1].y = area.bottom;
	triver[1].Red = GetRValue(header_ctrl_second_color) << 8;
	triver[1].Green = GetGValue(header_ctrl_second_color) << 8;
	triver[1].Blue = GetBValue(header_ctrl_second_color) << 8;
	triver[1].Alpha = 0;

	GRADIENT_RECT gRt = { 0,1 };

	GradientFill(cdc.m_hDC, triver, 2, &gRt, 1, GRADIENT_FILL_RECT_V);
}

void VersatileListColumnItem::Draw(CDC &cdc, CRect &area, CWnd &drawTarget, HDITEM &item)
{
	maybeDrawBackground(cdc, area);
	cdc.SetBkMode(TRANSPARENT);
	cdc.SelectObject(drawTarget.GetFont());
	cdc.SetTextColor(header_ctrl_text_color);

	CPen pen(PS_SOLID, 2, RGB(128, 128, 128));
	CPen *oldPen = cdc.SelectObject(&pen);

	cdc.MoveTo(area.right - 1, area.top);
	cdc.LineTo(area.right - 1, area.bottom);
	cdc.SelectObject(oldPen);
	if (item.pszText)
	{
		area.left += 5;
		area.top += 2;
		cdc.DrawText(item.pszText, area, DT_SINGLELINE | DT_WORD_ELLIPSIS);
	}
}




