
#ifndef ABSTRACT_LIST_MODEL_H__
#define ABSTRACT_LIST_MODEL_H__

#include <functional>
#include <vector>
#include <memory>

class AbstractListItem;

class AbstractListModel
{
private:
	std::vector<std::unique_ptr<AbstractListItem>> m_itemVector;
	uint32_t m_textColor;

public:
	AbstractListModel();
	virtual ~AbstractListModel();

	template <class Ty, typename std::enable_if<std::is_base_of<AbstractListItem, Ty>::value, bool>::type = true>
	Ty *GetItem(int index) const
	{
		if (static_cast<size_t>(index) >= m_itemVector.size())
			return {};

		return static_cast<Ty *>(m_itemVector[index].get());
	}
	void Push(std::unique_ptr<AbstractListItem> item);
	template <class Ty, class... Args>
	void Emplace(Args&&... args)
	{
		static_assert(std::is_base_of<AbstractListItem, Ty>::value, "inherit");
		m_itemVector.push_back(std::make_unique<Ty>(std::forward<Args>(args)...));
	}
	virtual uint32_t TextColor() const
	{
		return m_textColor;
	}
	void SetTextColor(uint32_t colr)
	{
		m_textColor = colr;
	}
	template <class Ty, typename std::enable_if<std::is_base_of<AbstractListItem, Ty>::value, bool>::type = true>
	void PreOrder(std::function<void(Ty &)> &&fn)
	{
		for (auto &item : m_itemVector)
			fn(*static_cast<Ty *>(item.get()));
	}
};

#endif

