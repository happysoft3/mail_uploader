
#ifndef FILE_STATUS_ITEM_PACK_H__
#define FILE_STATUS_ITEM_PACK_H__

#include "versatileListModel.h"
#include <string>

struct FileStatusData;

class FileStatusItemPack : public VersatileListModel
{
private:
	std::shared_ptr<FileStatusData> m_statusData;

public:
	FileStatusItemPack();
	~FileStatusItemPack() override;

public:
	void PutFileItem(const std::string &url, size_t fileSize, int index);
	std::shared_ptr<FileStatusData> GetStatus();

private:
	uint32_t TextColor() const override;
};

#endif

