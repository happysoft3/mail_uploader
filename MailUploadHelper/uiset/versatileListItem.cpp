
#include "pch.h"
#include "versatileListItem.h"

VersatileListItem::VersatileListItem(const std::string &desc)
	: AbstractListItem()
{
	m_description = desc;
}

VersatileListItem::~VersatileListItem()
{ }

void VersatileListItem::ListviewInfoText(LV_ITEM *pItem)
{
	std::string text;

	if (m_description.size())
		lstrcpyn(pItem->pszText, m_description.c_str(), pItem->cchTextMax);
}


