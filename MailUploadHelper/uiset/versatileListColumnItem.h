
#ifndef VERSATILE_LIST_COLUMN_ITEM_H__
#define VERSATILE_LIST_COLUMN_ITEM_H__

#include "abstractListItem.h"
#include <string>

class VersatileListColumnItem : public AbstractListItem
{
private:
	int m_width;
	std::string m_name;

public:
	VersatileListColumnItem();
	VersatileListColumnItem(const std::string &name, int width);
	~VersatileListColumnItem() override;

public:
	int Width() const
	{
		return m_width;
	}
	const char *const Name() const
	{
		return m_name.empty() ? "" : m_name.c_str();
	}
	virtual int Format() const;

private:
	void maybeDrawBackground(CDC &cdc, const CRect &area);

public:
	virtual void Draw(CDC &cdc, CRect &area, CWnd &drawTarget, HDITEM &item);
};

#endif
