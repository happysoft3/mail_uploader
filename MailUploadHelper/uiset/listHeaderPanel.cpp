
#include "pch.h"
#include "listHeaderPanel.h"
#include "uiset/versatileListColumn.h"
#include "uiset/versatileListColumnItem.h"
#include "cbufferdc.h"

IMPLEMENT_DYNAMIC(ListHeaderPanel, CHeaderCtrl)

ListHeaderPanel::ListHeaderPanel()
	: CHeaderCtrl()
{
	m_colCount = 0;
	m_curIndex = 0;
	m_validWidth = 0;
}

ListHeaderPanel::~ListHeaderPanel()
{ }

void ListHeaderPanel::drawImpl(CDC &cdc, VersatileListColumnItem &item)
{
	CRect area;

	GetItemRect(m_curIndex, &area);

	HDITEM detail = {};
	TCHAR txtBuffer[256] = {};

	detail.pszText = txtBuffer;
	detail.cchTextMax = sizeof(txtBuffer) - 1;
	detail.mask = HDI_TEXT | HDI_FORMAT | HDI_ORDER | HDI_WIDTH;
	GetItem(m_curIndex, &detail);
	area.left = m_validWidth;
	area.right = area.left + detail.cxy;
	m_validWidth = area.right;

	item.Draw(cdc, area, *this, detail);
	++m_curIndex;
}

void ListHeaderPanel::Draw(CDC &cdc)
{
	if (!m_columnData)
		return;

	m_colCount = GetItemCount();
	m_curIndex = 0;
	m_validWidth = 0;
	m_columnData->PreOrder<VersatileListColumnItem>([this, &cdc](VersatileListColumnItem &i)
	{
		drawImpl(cdc, i);
	});
}

void ListHeaderPanel::AttachColumn(std::unique_ptr<VersatileListColumn> column)
{
	m_columnData = std::move(column);
	//this->Invalidate();	//maybe require
}

BEGIN_MESSAGE_MAP(ListHeaderPanel, CHeaderCtrl)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

void ListHeaderPanel::OnPaint()
{
	CBufferDC cdc(this);

	Draw(cdc);
}

BOOL ListHeaderPanel::OnEraseBkgnd(CDC *pDC)
{
	return TRUE;
}
