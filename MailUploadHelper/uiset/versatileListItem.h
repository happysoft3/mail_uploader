
#ifndef VERSATILE_LIST_ITEM_H__
#define VERSATILE_LIST_ITEM_H__

#include "abstractListItem.h"
#include <string>

class VersatileListItem : public AbstractListItem
{
private:
	std::string m_description;

public:
	VersatileListItem(const std::string &desc = {});
	virtual ~VersatileListItem();

public:
	const char *const Description() const
	{
		return m_description.empty() ? "" : m_description.c_str();
	}
	void SetDescription(const std::string &desc)
	{
		m_description = desc;
	}
	virtual void ListviewInfoText(LV_ITEM *pItem);
	virtual void ListviewInfoImage(LV_ITEM *pItem)
	{ }
};

#endif
