
#include "pch.h"
#include "versatileListColumn.h"
#include "versatileListColumnItem.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

VersatileListColumn::VersatileListColumn()
	: AbstractListModel()
{
	m_offset = 0;
	m_columns = 0;
}

VersatileListColumn::~VersatileListColumn()
{ }

void VersatileListColumn::Append(const std::string &name, int width)
{
	Push(std::make_unique<VersatileListColumnItem>(name, width));
}

void VersatileListColumn::insertElementItem(VersatileListColumnItem &item, CListCtrl &viewer, const CRect &area)
{
	int viewerWidth = area.Width();

	if (m_offset >= viewerWidth)
		return;

	int finalWidth = item.Width();

	if (m_offset + finalWidth > area.Width())
	{
		finalWidth = viewerWidth - m_offset;
	}
	viewer.InsertColumn(m_columns++, item.Name(), item.Format(), finalWidth);
	m_offset += finalWidth;
}

void VersatileListColumn::Fetch(CListCtrl &viewer)
{
	CRect viewArea;

	viewer.GetClientRect(viewArea);
	m_offset = 0;
	m_columns = 0;
	PreOrder<VersatileListColumnItem>([this, &viewer, &viewArea](VersatileListColumnItem &item)
	{
		insertElementItem(item, viewer, viewArea);
	});
}

