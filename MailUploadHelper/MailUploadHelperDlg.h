﻿
// MailUploadHelperDlg.h: 헤더 파일
//

#pragma once

#include "logListView.h"
#include "fileListView.h"
#include "uiset/groupBox.h"
#include "dialog/prettyButton.h"
#include <string>
#include <memory>
#include <functional>

class CoreUi;
class ParameterLoader;
class LogQueue;

// CMailUploadHelperDlg 대화 상자
class CMailUploadHelperDlg : public CDialogEx
{
private:
	PrettyButton m_abortBtn;
	//std::function<void()> m_abortBtnFn;
	CProgressCtrl m_progressTube;
	LogListView m_logList;
	FileListView m_fileList;
	CBitmap m_bkBmp;
	CSize m_bkbmpSize;
	GroupBox m_mainBoundary;

	std::unique_ptr<CoreUi> m_coreui;
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::shared_ptr<LogQueue> m_logQueue;

// 생성입니다.
public:
	CMailUploadHelperDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.
	~CMailUploadHelperDlg() override;

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MAILUPLOADHELPER_DIALOG };
#endif

private:
	void initialCallOnce();
	void drawMainWindow(CDC &cdc);
	void initialLoadImage();
	void initialControls();
	bool initAfterUiLoaded();
	void uploadLogWithParamResult();
	void updateLogList();
	void endUploading();
	void updateProgressTube(int c);
	void doStart();
	void doAbort();
	void doClose();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

private:
	BOOL PreTranslateMessage(MSG *pMsg);

protected:
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg BOOL OnEraseBkgnd(CDC *pDC);
	afx_msg void OnPaint();
	//afx_msg void OnClickAbort();
	afx_msg void OnDestroy();
	afx_msg void OnClose();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnReceiveFromCoreUI(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};
