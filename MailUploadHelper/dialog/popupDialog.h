
#ifndef POPUP_DIALOG_H__
#define POPUP_DIALOG_H__

#include "textLabel.h"
#include "prettyButton.h"

class PopupDialog : public CDialogEx
{
	DECLARE_DYNAMIC(PopupDialog)

private:
	TextLabel m_textLabel;
	CWnd *m_parentWindow;
	PrettyButton m_closeBtn;

public:
	PopupDialog(CWnd *parentWnd = nullptr);
	~PopupDialog() override;

private:
	void windowCentering();
	void hideThisWindow();

protected:
	void DoDataExchange(CDataExchange *pDX) override;
	BOOL OnInitDialog() override;

public:
	void SetBodyText(const std::string &text);
	void Show();

protected:
	DECLARE_MESSAGE_MAP()
	BOOL PreTranslateMessage(MSG *pMsg);
	void OnActivate(UINT nState, CWnd *pWndOther, BOOL bMinimized);
};

#endif

