
#include "pch.h"
#include "popupDialog.h"
#include "resource.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IMPLEMENT_DYNAMIC(PopupDialog, CDialogEx)

PopupDialog::PopupDialog(CWnd *parentWnd)
	: CDialogEx()
{
	m_parentWindow = parentWnd;
	Create(IDD_POPUP_DIALOG, parentWnd);
}

PopupDialog::~PopupDialog()
{ }

void PopupDialog::windowCentering()
{
	if (!m_parentWindow)
		return;

	CRect parentArea, selfArea;

	m_parentWindow->GetWindowRect(&parentArea);
	GetWindowRect(&selfArea);

	int xpos = ((parentArea.right - parentArea.left) / 2) - ((selfArea.right - selfArea.left) / 2);
	int ypos = ((parentArea.bottom - parentArea.top) / 2) - ((selfArea.bottom - selfArea.top) / 2);

	MoveWindow(parentArea.left + xpos, parentArea.top + ypos, selfArea.Width(), selfArea.Height(), false);
}

void PopupDialog::hideThisWindow()
{
	ShowWindow(SW_HIDE);
}

void PopupDialog::DoDataExchange(CDataExchange *pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_POPUP_TEXT, m_textLabel);
	DDX_Control(pDX, IDC_POPUP_CLOSE, m_closeBtn);
}

BOOL PopupDialog::OnInitDialog()
{
	auto ret = CDialogEx::OnInitDialog();

	m_closeBtn.SetCallback([this]()
	{
		hideThisWindow();
	});
	m_closeBtn.ModifyWndName("CLOSE");
	CenterWindow();
	return ret;
}

void PopupDialog::SetBodyText(const std::string &text)
{
	m_textLabel.SetWindowTextA(toArray(text));
}

void PopupDialog::Show()
{
	windowCentering();
	ShowWindow(SW_SHOW);
	SetForegroundWindow();
	SetActiveWindow();
	EnableWindow(true);
}

BEGIN_MESSAGE_MAP(PopupDialog, CDialogEx)
	ON_WM_ACTIVATE()
END_MESSAGE_MAP()

BOOL PopupDialog::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
		case VK_F1:
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}

void PopupDialog::OnActivate(UINT nState, CWnd *pWndOther, BOOL bMinimized)
{
	CDialogEx::OnActivate(nState, pWndOther, bMinimized);

	switch (nState)
	{
	case WA_INACTIVE:   //lost focus
		hideThisWindow();
		break;

	case WA_ACTIVE:     //get focus
	case WA_CLICKACTIVE:
		break;
	}
}

