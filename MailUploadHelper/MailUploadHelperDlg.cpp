﻿
// MailUploadHelperDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "MailUploadHelper.h"
#include "MailUploadHelperDlg.h"
#include "afxdialogex.h"

#include "coreui/coreUi.h"
#include "logQueue.h"
#include "coreui/loadParamResult.h"
#include "MailUploadLib/fileStatusResult.h"
#include "MailUploadLib/parameterLoader.h"
#include "uiset/versatileListModel.h"
#include "uiset/versatileListItem.h"
#include "cbufferdc.h"

#include <algorithm>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define HIDE_MAIN_FRAME_	//flicker 문제 때문에 추가합니다


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMailUploadHelperDlg 대화 상자



CMailUploadHelperDlg::CMailUploadHelperDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MAILUPLOADHELPER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_coreui = std::make_unique<CoreUi>(this);
}

CMailUploadHelperDlg::~CMailUploadHelperDlg()
{ }

void CMailUploadHelperDlg::initialCallOnce()
{
	m_logQueue = std::make_shared<LogQueue>();
	m_coreui->PutLogQueue(m_logQueue);
	m_coreui->TryLoadParam(theApp.m_lpCmdLine);		//파일 정보 로딩 작업->비동기 처리
}

void CMailUploadHelperDlg::drawMainWindow(CDC &cdc)
{
	CRect area;

#ifdef HIDE_MAIN_FRAME_
	m_logList.GetWindowRect(area);
	ScreenToClient(area);
	cdc.ExcludeClipRect(area);

	m_fileList.GetWindowRect(area);
	ScreenToClient(area);
	cdc.ExcludeClipRect(area);

#else
	m_mainBoundary.GetWindowRect(area);
	ScreenToClient(area);
	cdc.ExcludeClipRect(area);
#endif
	CDC memDC;

	memDC.CreateCompatibleDC(&cdc);
	CBitmap *pOldbitmap = memDC.SelectObject(&m_bkBmp);
	cdc.BitBlt(0, 0, m_bkbmpSize.cx, m_bkbmpSize.cy, &memDC, 0, 0, SRCCOPY);
	memDC.SelectObject(pOldbitmap);
	memDC.DeleteDC();
	//CBrush brush;
	//CRect clip;

	//cdc.GetClipBox(clip);
	//brush.CreateSolidBrush(RGB(60, 60, 119));
	//cdc.FillRect(clip, &brush);
}

void CMailUploadHelperDlg::initialLoadImage()
{
	m_bkBmp.LoadBitmapA(IDB_BITMAP1);
	BITMAP bm;
	m_bkBmp.GetBitmap(&bm);
	m_bkbmpSize = CSize(bm.bmWidth, bm.bmHeight);
}

void CMailUploadHelperDlg::initialControls()
{
	SetWindowText("Upload Manager");
	m_abortBtn.ModifyWndName("Upload");
	m_abortBtn.SetCallback([this]()
	{
		doStart();
	});
	m_progressTube.ShowWindow(SW_HIDE);
	m_progressTube.SetBarColor(RGB(0, 241, 32));
	m_progressTube.SetBkColor(RGB(238, 238, 238));
	m_mainBoundary.PutOverayWindow(&m_logList);
	m_mainBoundary.PutOverayWindow(&m_fileList);
#ifdef HIDE_MAIN_FRAME_
	m_mainBoundary.ShowWindow(SW_HIDE);
#endif
	initialLoadImage();
}

bool CMailUploadHelperDlg::initAfterUiLoaded()
{
	//print log
	return true;
}

void CMailUploadHelperDlg::uploadLogWithParamResult()
{
	std::unique_ptr<TaskResult> result;

	try
	{
		result = m_coreui->FetchParam();
		if (!result)
			throw std::exception("no result");
	}
	catch (std::exception &e)
	{
		return;
	}

	LoadParamResult *convResult = dynamic_cast<LoadParamResult *>(result.get());
	std::list<std::unique_ptr<VersatileListModel>> elemList;

	convResult->StealColumnData(elemList);
	//m_fileList.Append(elemList);
	m_fileList.PutFileList(elemList);
	m_progressTube.SetRange(0, convResult->FileCount());
}

void CMailUploadHelperDlg::updateLogList()
{
	std::list<std::unique_ptr<VersatileListModel>> elemList;

	m_logQueue->Steal(elemList);
	if (elemList.size())
		m_logList.Append(elemList);
}

void CMailUploadHelperDlg::endUploading()
{
	m_fileList.UpdateFileStatus(m_coreui->CompleteUpload());
	m_abortBtn.SetCallback([this]()
	{
		doClose();
	});
	m_abortBtn.ModifyWndName("Close");
	m_abortBtn.Invalidate();
}

void CMailUploadHelperDlg::updateProgressTube(int c)
{
	m_progressTube.SetPos(c);
}

void CMailUploadHelperDlg::doStart()
{
	if (!m_coreui->DoUpload())
		return;

	m_abortBtn.SetCallback([this]()
	{
		doAbort();
	});
	m_abortBtn.ModifyWndName("abort");
	m_progressTube.ShowWindow(SW_SHOW);
}

void CMailUploadHelperDlg::doAbort()
{
	if (m_coreui)
		m_coreui->DoAbort();
}

void CMailUploadHelperDlg::doClose()
{
	PostMessage(WM_CLOSE, 0, 0);
}

void CMailUploadHelperDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	
	DDX_Control(pDX, IDC_UPLOAD_BUTTON, m_abortBtn);
	DDX_Control(pDX, IDC_UPLOAD_PROGRESS, m_progressTube);
	DDX_Control(pDX, IDC_UPLOAD_LIST, m_fileList);
	DDX_Control(pDX, IDC_UPLOAD_LOG, m_logList); //IDC_UPLOAD_PANEL
	DDX_Control(pDX, IDC_MAIN_FRAME, m_mainBoundary);
}

BEGIN_MESSAGE_MAP(CMailUploadHelperDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_MESSAGE(CoreUi::DispatchMsgCoreUi, OnReceiveFromCoreUI)
	//ON_BN_CLICKED(IDC_UPLOAD_BUTTON, OnClickAbort)
END_MESSAGE_MAP()

BOOL CMailUploadHelperDlg::PreTranslateMessage(MSG *pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_RETURN:
		case VK_ESCAPE:
		case VK_F1:
			return TRUE;
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


// CMailUploadHelperDlg 메시지 처리기

BOOL CMailUploadHelperDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.
	initialCallOnce();
	initialControls();

	return initAfterUiLoaded();  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CMailUploadHelperDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

BOOL CMailUploadHelperDlg::OnEraseBkgnd(CDC *pDC)
{
	drawMainWindow(*pDC);
	return TRUE;
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CMailUploadHelperDlg::OnPaint()
{
	Default();
}

//void CMailUploadHelperDlg::OnClickAbort()
//{
//	m_abortBtnFn();
//}

void CMailUploadHelperDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	m_bkBmp.DeleteObject();
}

void CMailUploadHelperDlg::OnClose()
{
	doAbort();
	CDialogEx::OnClose();
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CMailUploadHelperDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

HRESULT CMailUploadHelperDlg::OnReceiveFromCoreUI(WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{
	case CoreUi::MessageId::LoadedParam:
		uploadLogWithParamResult();
		break;

	case CoreUi::MessageId::PushLogMessage:
		updateLogList();
		break;

	case CoreUi::MessageId::EndUpload:
		endUploading();
		break;

	case CoreUi::MessageId::ReportProgress:
		updateProgressTube(static_cast<int>(lParam));
		break;

	default:
		break;
	}
	return 0;
}
