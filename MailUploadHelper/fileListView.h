
#ifndef FILE_LIST_VIEW_H__
#define FILE_LIST_VIEW_H__

#include "uiset/versatileListViewer.h"
#include <vector>

class FileStatusResult;
class FileStatusItemPack;
struct FileStatusData;

class FileListView : public VersatileListViewer
{
	DECLARE_DYNAMIC(FileListView)
private:
	std::vector<std::weak_ptr<FileStatusData>> m_states;

public:
	FileListView();
	~FileListView() override;

private:
	void PreSubclassWindow() override;

public:
	void UpdateFileStatus(std::unique_ptr<FileStatusResult> stateResult);
	void PutFileList(std::list<std::unique_ptr<VersatileListModel>> &files);

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnNcCalcSize(BOOL bCalcValidRects, NCCALCSIZE_PARAMS FAR* lpncsp);
};

#endif

