
#include "pch.h"
#include "logQueue.h"
#include "uiset/versatileListModel.h"
#include "uiset/versatileListItem.h"
#include <chrono>
#include <ctime>

LogQueue::LogQueue()
{ }

LogQueue::~LogQueue()
{ }

void LogQueue::writeTime(VersatileListItem &logItem)
{
	std::chrono::system_clock::time_point n = std::chrono::system_clock::now();
	time_t currentTime = std::chrono::system_clock::to_time_t(n);
	char buffer[256] = { };

	ctime_s(buffer, sizeof(buffer), &currentTime);
	logItem.SetDescription(buffer);
}

void LogQueue::Push(std::unique_ptr<VersatileListModel> log)
{
	std::lock_guard<std::mutex> guard(m_lock);
	m_logs.push_back(std::move(log));
}

void LogQueue::MakePush(const std::string &msg)
{
	auto elem = std::make_unique<VersatileListModel>();

	elem->Emplace<VersatileListItem>(msg);
	auto timeElem = std::make_unique<VersatileListItem>();
	
	writeTime(*timeElem);
	elem->Push(std::move(timeElem));

	Push(std::move(elem));
}

void LogQueue::Steal(std::list<std::unique_ptr<VersatileListModel>> &steal)
{
	for (auto &elem : m_logs)
		steal.push_back(std::move(elem));

	m_logs.clear();
}
