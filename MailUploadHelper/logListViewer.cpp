
#include "pch.h"
#include "loglistviewer.h"
#include "listheaderpanel.h"
#include "listElement.h"
#include "cbufferdc.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

IMPLEMENT_DYNAMIC(LogListViewer, CListCtrl)

static constexpr UINT s_list_base_color = RGB(9, 29, 45);

LogListViewer::LogListViewer()
	: CListCtrl()
{
	m_headerPanel = std::make_unique<ListHeaderPanel>();
	m_elementVec.reserve(65536);
	m_selectedColumn = -1;
}

LogListViewer::~LogListViewer()
{ }

void LogListViewer::AttachListColumn(const LogListViewer::ListColumn &columnData)
{
	int col = 0;

	for (const auto &column : columnData.m_columns)
		InsertColumn(col++, toArray(std::get<0>(column)), LVCFMT_LEFT | LVCFMT_FIXED_WIDTH, std::get<1>(column));
}

void LogListViewer::SetDoubleClickCallback(std::function<void(int, int)> onDbClick)
{
	m_onDbClick = onDbClick;
}

bool LogListViewer::ListPulling(const size_t inputSize)
{
	std::unique_lock<std::recursive_mutex> locker(m_lock);

	const size_t totalSize = m_elementVec.size() + inputSize;

	if (totalSize > m_elementVec.capacity())
	{
		const size_t pullCount = totalSize - m_elementVec.capacity();

		std::copy(m_elementVec.cbegin() + pullCount, m_elementVec.cend(), m_elementVec.begin());
		m_elementVec.resize(m_elementVec.size() - pullCount);
		return true;
	}
	return false;
}

void LogListViewer::UpdateListItem(const size_t count, bool isFull)
{
	switch (isFull)
	{
	case false:
	{
		bool selEnd = (m_selectedColumn >= 0) ? (GetItemCount() - 1 == m_selectedColumn) : false;

		SetItemCountEx(count, LVSICF_NOINVALIDATEALL | LVSICF_NOSCROLL);
		if (selEnd)
		{
			SetItemState(GetItemCount() - 1, LVNI_SELECTED | LVNI_FOCUSED, 0xf);
			EnsureVisible(GetItemCount() - 1, false);
		}
		break;
	}
	case true:
	{
		int topIndex = GetTopIndex();
		size_t last = topIndex + GetCountPerPage();

		SetRedraw(false);
		while (last--)
		{
			if (last < count)
				Update(last);
		}
		SetRedraw(true);
	}
	}
}

void LogListViewer::Append(LogListViewer::list_element_ty &elem)
{
	AppendList({ elem });
}

void LogListViewer::AppendList(const std::list<LogListViewer::list_element_ty> &addList)
{
	bool isFull = ListPulling(addList.size());
	size_t elemCount = 0;

	{
		std::unique_lock<std::recursive_mutex> locker(m_lock);

		for (const auto &element : addList)
			m_elementVec.push_back(element);

		elemCount = m_elementVec.size();
	}
	UpdateListItem(elemCount, isFull);
}

void LogListViewer::EnableHighlighting(HWND hWnd, int row, bool bHighlight)
{
	ListView_SetItemState(hWnd, row, bHighlight ? 0xff : 0, LVIS_SELECTED);
}

bool LogListViewer::IsRowSelected(HWND hWnd, int row)
{
	return ListView_GetItemState(hWnd, row, LVIS_SELECTED) != 0;
}

bool LogListViewer::IsRowHighlighted(HWND hWnd, int row)
{
	return IsRowSelected(hWnd, row);
}

void LogListViewer::drawListCell(CDC &cdc)
{
	CRect area;
	GetClientRect(area);
	CHeaderCtrl *pHeaderCtrl = GetHeaderCtrl();

	if (pHeaderCtrl)
	{
		CRect headerArea;

		pHeaderCtrl->GetWindowRect(headerArea);
		area.top += headerArea.Height();
	}
	CRect fieldArea;
	int itemCount = GetItemCount();

	if (itemCount > 0)
	{
		CPoint point;
		CRect itemArea;

		GetItemRect(itemCount - 1, itemArea, LVIR_BOUNDS);
		GetItemPosition(itemCount - 1, &point);
		fieldArea.top = area.top;
		fieldArea.left = point.x;
		fieldArea.right = itemArea.right;
		fieldArea.bottom = itemArea.bottom;

		if (GetExtendedStyle() & LVS_EX_CHECKBOXES)
			fieldArea.left -= GetSystemMetrics(SM_CXEDGE) + 16;
	}
	CBrush brush;

	brush.CreateSolidBrush(s_list_base_color);
	cdc.FillRect(area, &brush);
	if (!fieldArea.IsRectEmpty())
	{
		if (fieldArea.left > area.left)
			cdc.FillRect(CRect(0, area.top, fieldArea.left, area.bottom), &brush);
		if (fieldArea.bottom < area.bottom) // fill bottom rectangle
			cdc.FillRect(CRect(0, fieldArea.bottom, area.right, area.bottom), &brush);
		if (fieldArea.right < area.right) // fill right rectangle
			cdc.FillRect(CRect(fieldArea.right, area.top, area.right, area.bottom), &brush);
	}
}

void LogListViewer::drawListBackground(CDC &cdc)
{
	auto *pHeaderCtrl = GetHeaderCtrl();

	if (pHeaderCtrl)
	{
		CRect headerArea;

		pHeaderCtrl->GetWindowRect(headerArea);
		ScreenToClient(headerArea);
		cdc.ExcludeClipRect(headerArea);
	}
	CRect clip;

	cdc.GetClipBox(clip);
	cdc.FillSolidRect(clip, s_list_base_color);
	//DefWindowProc(WM_PAINT, (WPARAM)cdc.m_hDC, (LPARAM)0);
}

//void LogListViewer::Draw(CDC &cdc)
//{
//	drawStuff(cdc);
//}

void LogListViewer::PreSubclassWindow()
{
	SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);
	ModifyStyle(0, LVS_SINGLESEL);

	CHeaderCtrl *pHeaderCtrl = GetHeaderCtrl();
	HWND hWnd = reinterpret_cast<HWND>(::SendMessage(m_hWnd, LVM_GETHEADER, 0, 0));

	pHeaderCtrl->ModifyStyle(0, LVS_OWNERDRAWFIXED, SWP_FRAMECHANGED);
	m_headerPanel->SubclassWindow(hWnd);

	CListCtrl::PreSubclassWindow();
}

BEGIN_MESSAGE_MAP(LogListViewer, CListCtrl)
	ON_NOTIFY_REFLECT(LVN_GETDISPINFO, OnGetDisplayInfoList)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnNMCustomdraw)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnNMDbClick)
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

BOOL LogListViewer::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT *pResult)
{
	switch (((NMHDR *)lParam)->code)
	{
	case HDN_BEGINTRACKW:
	case HDN_BEGINTRACKA:
		*pResult = TRUE;

		return TRUE;
	}

	return CListCtrl::OnNotify(wParam, lParam, pResult);
}

void LogListViewer::OnGetDisplayInfoList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LV_DISPINFO *pDispInfo = reinterpret_cast<LV_DISPINFO *>(pNMHDR);
	LV_ITEM *pItem = &(pDispInfo->item);
	int itemId = pItem->iItem;

	if (pItem->mask & LVIF_TEXT)
	{
		std::string text;
		{
			std::unique_lock<std::recursive_mutex> locker(m_lock);

			text = m_elementVec[itemId]->GetElement(pItem->iSubItem);
		}

		if (text.size())
			lstrcpyn(pItem->pszText, text.c_str(), pItem->cchTextMax);
	}
	if (pItem->mask & LVIF_IMAGE)
	{
	}

	*pResult = 0;
}

void LogListViewer::OnNMCustomdraw(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	NMTVCUSTOMDRAW *pLVCD = reinterpret_cast<NMTVCUSTOMDRAW *>(pNMHDR);
	static bool bHighlighted = false;

	*pResult = CDRF_DODEFAULT;
	if (CDDS_PREPAINT == pNMCD->dwDrawStage)
	{
		*pResult = CDRF_NOTIFYITEMDRAW;
	}
	else if (CDDS_ITEMPREPAINT == pNMCD->dwDrawStage)
	{
		int iRow = static_cast<int>(pNMCD->dwItemSpec);

		bHighlighted = IsRowHighlighted(m_hWnd, iRow);

		if (bHighlighted)
		{
			pNMCD->uItemState = CDIS_DEFAULT;
			pLVCD->clrText = RGB(32, 194, 44); // Use my foreground hilite color
			pLVCD->clrTextBk = RGB(192, 0, 208); // Use my background hilite color
			if (m_selectedColumn != iRow)
				m_selectedColumn = iRow;
		}
		else
		{
			std::unique_lock<std::recursive_mutex> locker(m_lock);

			pLVCD->clrText = m_elementVec[iRow]->GetTextColor();
			pLVCD->clrTextBk = s_list_base_color;
		}

		*pResult = CDRF_DODEFAULT | CDRF_NOTIFYPOSTPAINT;
	}
	else if (CDDS_ITEMPOSTPAINT == pNMCD->dwDrawStage)
	{
		if (bHighlighted)
		{
			int iRow = static_cast<int>(pNMCD->dwItemSpec);

			EnableHighlighting(m_hWnd, iRow, true);
		}
		*pResult = CDRF_DODEFAULT;
	}
}

void LogListViewer::OnNMDbClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE lpnmItem = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	int columnId = lpnmItem->iItem;
	int rowId = lpnmItem->iSubItem;

	if (columnId != -1)
	{
		if (m_onDbClick)
			m_onDbClick(columnId, rowId);
	}
}

BOOL LogListViewer::OnEraseBkgnd(CDC *pDC)
{
	drawListBackground(*pDC);

	return TRUE;
}

void LogListViewer::OnPaint()
{
	CBufferDC cdc(this);

	drawListCell(cdc);

	ListHeaderPanel *pHeaderCtrl = dynamic_cast<ListHeaderPanel *>(GetHeaderCtrl());

	if (pHeaderCtrl)
		pHeaderCtrl->Draw(cdc);
}

