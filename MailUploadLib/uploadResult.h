
#ifndef UPLOAD_RESULT_H__
#define UPLOAD_RESULT_H__

#include "common/task/taskResult.h"

struct UploadResultData
{
	std::string m_serverFileUrl;
	std::string m_errorMessage;
	std::string m_localFileUrl;
	std::shared_ptr<std::exception> m_logicFailure;
};

class UploadResult : public TaskResult
{
private:
	std::unique_ptr<UploadResultData> m_data;

public:
	UploadResult(TaskUnit &task);
	~UploadResult() override;

public:
	std::unique_ptr<UploadResultData> CloneData() const;
	const char *const ServerFileURL() const
	{
		return m_data->m_serverFileUrl.c_str();
	}
	void SetServerFileURL(const std::string &url)
	{
		m_data->m_serverFileUrl = url;
	}
	const char *const ErrorMessage() const
	{
		return m_data->m_errorMessage.c_str();
	}
	void PutErrorMessage(const std::string &error)
	{
		m_data->m_errorMessage = error;
	}
	const char *const LocalFileURL() const
	{
		return m_data->m_localFileUrl.c_str();
	}
	void SetLocalFileURL(const std::string &url)
	{
		m_data->m_localFileUrl = url;
	}
};

#endif

