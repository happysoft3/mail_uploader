
#include "uploadManager.h"
#include "joinTaskThread.h"
#include "parameterLoader.h"
#include "fileStatusResult.h"
#include "common/task/taskQueue.h"
#include "uploadTask.h"
#include "common/task/taskResultHash.h"
#include "uploadResult.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"

#include <filesystem>

using namespace _StringHelper;

class UploadTaskControl : public TaskUnit::Controller
{	//아직 아무것도 하지 않아요...
public:
	UploadTaskControl()
		: TaskUnit::Controller()
	{ }
	~UploadTaskControl() override
	{ }
};

struct UploadManager::Core
{
	std::shared_ptr<UploadTaskControl> m_taskControl;
};

UploadManager::UploadManager()
	: CCObject(), m_core(new Core)
{
	m_progress = 0;
	m_core->m_taskControl = std::make_shared<UploadTaskControl>();
}

UploadManager::~UploadManager()
{
	m_joinThread->Wait();
}

void UploadManager::createUploadTask(const std::string &fullPath, int index)
{
	//if (!std::experimental::filesystem::exists(fullPath))
	//{
	//	logPrintOut(stringFormat("has no attrib file '%s'", fullPath));
	//	return;
	//}
		//MY_THROW() << stringFormat("has no attrib file '%s'", fullPath);

	auto task = std::make_unique<UploadTask>(m_core->m_taskControl);

	m_procResult->PutID(*task, index);
	task->OnTaskStart().Connection(&UploadManager::onTaskStart, this);
	task->OnEndedTask().Connection(&UploadManager::onTaskEnd, this);
	task->PutParams(*m_paramLoader, fullPath);
	m_taskQueue->Push(std::move(task));
}

void UploadManager::onTaskStart(const std::string &fn)
{
	MY_PRINT() << "start task: " << fn;
	logPrintOut("start task: ", fn);
}

void UploadManager::onTaskEnd(std::shared_ptr<UploadResultData> res)
{
	MY_PRINT() << "end task: " << res->m_localFileUrl;
	logPrintOut("end task: ", res->m_localFileUrl);
	if (res->m_logicFailure)
	{
		MY_PRINT() << "task error: " << res->m_logicFailure->what();
		MY_PRINT() << "fail message: " << res->m_errorMessage;
		logPrintOut("task error: ", res->m_logicFailure->what());
	}
	m_OnReportProgress.Emit(++m_progress);
}

void UploadManager::onEndJoinThread()
{
	MY_PRINT() << "halted join thread";
	logPrintOut("end of process");
	m_OnReportEndProgress.Emit();
}

void UploadManager::PrematurePreparation(std::unique_ptr<ParameterLoader> paramLoader)
{
	m_procResult = std::make_unique<FileStatusResult>();
	m_taskQueue = std::make_unique<TaskQueue>();
	m_paramLoader = std::move(paramLoader);
	using namespace std::placeholders;
	m_paramLoader->PreorderFileList(std::bind(&UploadManager::createUploadTask, this, _1, _2));
}

void UploadManager::DoUpload()
{
	if (!m_taskQueue)
		MY_THROW() << "queue==null";

	if (m_joinThread)
		MY_THROW() << "join thread";

	m_joinThread = std::make_unique<JoinTaskThread>();
	m_joinThread->OnJoinFinish().Connection(&UploadManager::onEndJoinThread, this);
	m_joinThread->Perform(std::move(m_taskQueue));
}

void UploadManager::computeErrorCount(TaskResult &result, int &errorCount)
{
	auto err = result.GetError();

	if (err)
		++errorCount;
	m_procResult->PutStatus(result, err ? "NG" : "OK");
}

std::unique_ptr<FileStatusResult> UploadManager::CheckResult()
{
	auto resultMap = m_joinThread->GetResult();
	int countOfProc = resultMap->Count();
	int errorCount = 0;

	resultMap->Preorder([this, &errorCount](TaskResult &r)
	{
		computeErrorCount(r, errorCount);
	});

	std::string statistics = stringFormat("End Of Process (task: %d, error: %d)", countOfProc, errorCount);

	MY_PRINT() << statistics;
	logPrintOut(statistics);
	return std::move(m_procResult);
}

void UploadManager::CancelUpload()
{
	if (m_core->m_taskControl)
	{
		logPrintOut("cancel uploading");
		m_core->m_taskControl.reset();
	}
}
