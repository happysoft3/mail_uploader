
#ifndef TASK_RESULT_QUEUE_H__
#define TASK_RESULT_QUEUE_H__

#include <string>
#include <list>
#include <memory>
#include <map>

class AbstractTaskResult;

class TaskResultQueue
{
private:
	using result_queue_ty = std::list<std::unique_ptr<AbstractTaskResult>>;
	result_queue_ty m_resultQueue;

	using result_queue_iter = result_queue_ty::iterator;
	std::map<std::string, result_queue_iter> m_resultMap;

public:
	TaskResultQueue();
	~TaskResultQueue();

	void Push(std::unique_ptr<AbstractTaskResult> result);
	std::unique_ptr<AbstractTaskResult> Pop();
};

#endif

