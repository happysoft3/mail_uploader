
#ifndef ABSTRACT_TASK_H__
#define ABSTRACT_TASK_H__

#include <memory>

class AbstractTaskResult;

class AbstractTask
{
public:
	AbstractTask();
	virtual ~AbstractTask();

protected:
	virtual void perform() = 0;
	virtual std::unique_ptr<AbstractTaskResult> getTaskResult() { return {}; }
	std::unique_ptr<AbstractTaskResult> remotePerform(AbstractTask &task);
};

#endif

