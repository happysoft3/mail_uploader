
#ifndef PARAMETER_LOADER_H__
#define PARAMETER_LOADER_H__

#include "systemFileParser/iniFileMan.h"
#include <list>
#include <string>
#include <memory>
#include <functional>

class PathManager;

class ParameterLoader : public IniFileMan
{
private:
	std::unique_ptr<PathManager> m_basepathMan;
	std::list<std::string> m_fileList;
	std::string m_serverUrl;
	std::string m_userMailPath;
	std::string m_userAuthData;

public:
	ParameterLoader();
	~ParameterLoader() override;

private:
	void catchException(const std::exception &except) override;
	std::unique_ptr<PathManager> loadBasepath();
	void loadFileList();
	void loadServerUrl();
	static void fetchUserMailPath(const std::string &t, std::string &mailPath);
	void verySimpleParseOpenUrl(const std::string &t);
	void loadUserMailPath();

public:
	const char *const UserMailPath() const
	{
		return m_userMailPath.empty() ? "" : m_userMailPath.c_str();
	}
	const char *const UserAuthData() const
	{
		return m_userAuthData.c_str();
	}
	const char *const ServerURL() const;
	void PreorderFileList(std::function<void(const std::string &, int)> &&fn, bool full = true);
	bool ReadIni(const std::string &inifile) override;
	std::unique_ptr<PathManager> CreatePath(const std::string &subName) const;
	int LoadedCount() const
	{
		return static_cast<int>(m_fileList.size());
	}
};

#endif

