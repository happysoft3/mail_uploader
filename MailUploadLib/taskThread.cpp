
#include "taskThread.h"
#include "taskResultQueue.h"
#include "abstractTaskResult.h"
#include <thread>

TaskThread::TaskThread()
	: AbstractTask()
{ }

TaskThread::~TaskThread()
{
	m_restrictKey.reset();
}

std::unique_ptr<AbstractTaskResult> TaskThread::tryGetResult(bool &keep)
{
	keep = false;
	return {};
}

void TaskThread::onWorkerShutdown()
{
	if (m_cbShutdown)
	{
		(*m_cbShutdown)();
		m_cbShutdown.reset();
	}
	if (m_cbOneStep)
		m_cbOneStep.reset();
}

std::unique_ptr<TaskResultQueue> TaskThread::doTask(std::weak_ptr<bool> privateKey)
{
	auto resultCollection = std::make_unique<TaskResultQueue>();
	std::unique_ptr<AbstractTaskResult> result;
	bool keepgoing = beforeTasking();

	while (keepgoing)
	{
		result = tryGetResult(keepgoing);

		if (m_cbOneStep && result)
			(*m_cbOneStep)(*result);
		resultCollection->Push(std::move(result));
		if (privateKey.expired())
			break;
	}
	onWorkerShutdown();
	return resultCollection;
}

void TaskThread::perform()
{
	m_restrictKey = std::make_shared<bool>(false);
	std::packaged_task<task_result_ty()> task([this, pkey = std::weak_ptr<bool>(m_restrictKey)]() { return this->doTask(pkey); });
	m_archive = std::make_unique<std::future<task_result_ty>>(task.get_future());
	std::thread worker(std::move(task));

	worker.detach();
}

void TaskThread::SetKey(std::shared_ptr<bool> &key)
{
	m_enableKey = key;
}

std::unique_ptr<TaskResultQueue> TaskThread::FetchResult()
{
	if (!m_archive)
		return {};

	std::unique_ptr<TaskResultQueue> resQueue = m_archive->get();

	m_archive.reset();
	return resQueue;
}

void TaskThread::RegistCallbackOneStep(task_thread_cb_ty &&cb)
{
	m_cbOneStep = std::make_shared<task_thread_cb_ty>(std::forward<std::decay_t<decltype(cb)>>(cb));
}

void TaskThread::RegistCallbackShutdown(std::function<void()> &&cb)
{
	m_cbShutdown = std::make_shared<std::function<void()>>(std::forward<std::function<void()>>(cb));
}
