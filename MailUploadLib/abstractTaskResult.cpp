
#include "abstractTaskResult.h"
#include "common/utils/randomString.h"
#include "common/utils/stringhelper.h"
#include <mutex>

using namespace _StringHelper;

static int s_uniqueFirst = 0;
static int s_uniqueSecond = 0;
static int s_uniqueThird = 0;
constexpr int max_unique_value = 16777216;

AbstractTaskResult::AbstractTaskResult()
{
	m_uniqueId = publishUniqueId();
}

AbstractTaskResult::~AbstractTaskResult()
{ }

std::string AbstractTaskResult::publishUniqueId()
{
	std::string rndgen = "XXXXXXXX";
	static std::mutex s_lock;
	std::lock_guard<std::mutex> guard(s_lock);
	if (++s_uniqueFirst > max_unique_value)
	{
		s_uniqueFirst = 0;
		if (++s_uniqueSecond > max_unique_value)
		{
			s_uniqueSecond = 0;
			if (++s_uniqueThird > max_unique_value)
				throw std::logic_error("cannot allocate more unique number");
		}
	}
	RandomString::Generate(rndgen);
	return stringFormat("%s%d%d%d", rndgen, s_uniqueThird, s_uniqueSecond, s_uniqueFirst);
}

void AbstractTaskResult::SetData(const std::string &key, const std::string &value)
{
	if (key.empty())
		return;

	m_storedData[key] = value;
}

const char *const AbstractTaskResult::GetData(const std::string &key) const
{
	auto keyIter = m_storedData.find(key);

	if (keyIter == m_storedData.cend())
		return {};

	return keyIter->second.c_str();
}
