
#include "uploadResult.h"
#include "common/task/taskUnit.h"

UploadResult::UploadResult(TaskUnit &task)
	: TaskResult(task)
{
	m_data = std::make_unique<UploadResultData>();
}

UploadResult::~UploadResult()
{ }

std::unique_ptr<UploadResultData> UploadResult::CloneData() const
{
	auto d = std::make_unique<UploadResultData>(*m_data);
	auto taskErr = GetError();

	if (taskErr)
		d->m_logicFailure = std::move(taskErr);
	return d;
}
