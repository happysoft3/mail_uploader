
#include "taskQueue.h"
#include "abstractTask.h"
#include "abstractTaskResult.h"

TaskQueue::TaskQueue()
{ }

TaskQueue::~TaskQueue()
{ }

void TaskQueue::Push(std::unique_ptr<AbstractTask> task)
{
	std::lock_guard<std::mutex> lock(m_lock);
	m_taskList.push_back(std::move(task));
}

std::unique_ptr<AbstractTask> TaskQueue::Pop()
{
	std::lock_guard<std::mutex> lock(m_lock);
	if (m_taskList.empty())
		return {};

	std::unique_ptr<AbstractTask> task = std::move(m_taskList.front());

	m_taskList.pop_front();
	return task;
}
