
#include "abstractTask.h"
#include "abstractTaskResult.h"

AbstractTask::AbstractTask()
{ }

AbstractTask::~AbstractTask()
{ }

std::unique_ptr<AbstractTaskResult> AbstractTask::remotePerform(AbstractTask &task)
{
	task.perform();
	return task.getTaskResult();
}
