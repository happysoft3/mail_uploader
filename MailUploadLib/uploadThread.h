
#ifndef UPLOAD_THREAD_H__
#define UPLOAD_THREAD_H__

#include "common/task/taskThread.h"

class UploadThread : public TaskThread
{
public:
	UploadThread();
	~UploadThread() override;
};

#endif

