
#ifndef JOIN_TASK_THREAD_H__
#define JOIN_TASK_THREAD_H__

#include "common/task/taskThread.h"

class TaskQueue;
class UploadThread;

class JoinTaskThread : public TaskThread
{
private:
	std::vector<std::unique_ptr<UploadThread>> m_procVector;

public:
	JoinTaskThread();
	~JoinTaskThread() override;

private:
	void createNumofThread(size_t n);
	bool beforeExec(std::shared_ptr<TaskQueue> queue) override;
	void afterExec(TaskResultHash &resultMap) noexcept override;

	DECLARE_SIGNAL(OnJoinFinish)
};

#endif
