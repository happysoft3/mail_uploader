
#include "joinThread.h"
#include "taskResultQueue.h"
#include "abstractTaskResult.h"

JoinThread::JoinThread()
	: TaskThread()
{ }

JoinThread::~JoinThread()
{ }

bool JoinThread::beforeTasking()
{
	if (m_workerVector.empty())
		return false;

	m_wholeResultQueue = std::make_unique<TaskResultQueue>();
	return true;
}

//스레드로 부터 생성된, 결과를 축적합니다
void JoinThread::accumulateTaskResult(TaskThread &worker)
{
	auto queue = worker.FetchResult();
	std::unique_ptr<AbstractTaskResult> result;

	for (;;)
	{
		result = queue->Pop();

		if (!result)
			break;
		m_wholeResultQueue->Push(std::move(result));
	}
}

std::unique_ptr<AbstractTaskResult> JoinThread::tryGetResult(bool &keep)
{
	for (auto &worker : m_workerVector)
		accumulateTaskResult(*worker);

	keep = false;
	return {};
}

void JoinThread::AttachWorkerVector(std::vector<std::unique_ptr<TaskThread>> &&workers)
{
	if (workers.empty())
		throw std::logic_error("vector for worker is empty");

	m_workerVector = std::move(workers);
}

