#pragma once

#include <string>

class PocoRunner
{
public:
	PocoRunner();
	~PocoRunner();

	static std::string FileUpload(const std::string & url, const std::string & file, const std::string & name, std::string & errMsg);
};

