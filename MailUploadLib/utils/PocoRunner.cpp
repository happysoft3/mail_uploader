
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <filesystem>

#pragma comment(lib, "Iphlpapi.lib")

#include "PocoRunner.h"

#include "Poco/URI.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"

#include "json/json.h"

PocoRunner::PocoRunner()
{
}


PocoRunner::~PocoRunner()
{
}

std::string PocoRunner::FileUpload(const std::string & url, const std::string & sFile, const std::string & name, std::string & errMsg)
{
	////////////////////////////////////////////////////////////////////
// poco �̿�
	try
	{
		//Prepare request
		Poco::URI uri(url);
		const Poco::Net::Context::Ptr context = new Poco::Net::Context(Poco::Net::Context::CLIENT_USE, "", "", "", Poco::Net::Context::VERIFY_NONE, 9, false, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
		Poco::Net::HTTPClientSession *session = nullptr;

		if (uri.getScheme() == "https") {
			session = new Poco::Net::HTTPSClientSession(uri.getHost(), uri.getPort(), context);
			
		}
		else {
			session = new Poco::Net::HTTPClientSession(uri.getHost(), uri.getPort());
		}

		if (session == nullptr) {
			return "";
		}

		session->setKeepAlive(true);

		// prepare path
		std::string path(uri.getPathAndQuery());
		if (path.empty())
		{
			path = "/";
		}

		Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, path, Poco::Net::HTTPMessage::HTTP_1_1);
	
		req.setKeepAlive(true);
		
		// Set headers
		// Set Multipart type and some boundary value 
		req.setContentType("multipart/form-data; boundary=-------------------------87142694621188");

		// Append boundary, content type and disposition to file data
		std::string boundary = "-------------------------87142694621188";
		std::string data1("---------------------------87142694621188\r\nContent-Disposition: form-data; name=\"data\"; filename=\"");
		std::string data2(name);
		std::string data3("\";\r\nContent-Type: application/octet-stream\r\n\r\n");

		std::string data4("\r\n---------------------------87142694621188--\r\n");


		// Read File Data in binary
		std::experimental::filesystem::path filePath = std::experimental::filesystem::u8path(sFile);
		const auto fsize = std::experimental::filesystem::file_size(filePath);
		std::ifstream file(filePath, std::ios::binary);
		file.rdbuf();

		long nSendAllSize = 0;
		nSendAllSize += data1.length() + data2.length() + data3.length() + data4.length() + fsize;

		req.setContentLength(nSendAllSize);

		// sends request, returns open stream
		std::ostream& myOStream = session->sendRequest(req);

		// sends the body
		myOStream << data1;
		myOStream << data2;
		myOStream << data3;
		myOStream << file.rdbuf();
		myOStream << data4;

		Poco::Net::HTTPResponse res;
		std::istream& rs = session->receiveResponse(res);

		//Get status code
		int statusCode = (int)res.getStatus();

		//Get status
		std::string status = res.getReason();

		std::string response;
		while (rs)
		{
			response.push_back(char(rs.get()));
		}

		std::stringstream stream1;
		stream1 << "StatusCode: " << statusCode << "\nStatus: " << status << "\nResponse: " << response;
		errMsg = stream1.str();

		if (session) {
			delete session;
			session = nullptr;
		}

		if (statusCode == Poco::Net::HTTPResponse::HTTP_OK) {
			Json::Reader reader;
			Json::Value root;
			bool parsingRet = reader.parse(response, root);
			if (!parsingRet)
			{
				return "";
			}

			std::string sData(root["folder"].asString());
			return sData;
		}
	}
	catch (Poco::Exception& exception)
	{
		std::stringstream stream1;
		stream1 << "Exception occurred while uploading: " << exception.displayText();
		errMsg = stream1.str();
	}
	catch (std::exception & e)
	{
		std::stringstream stream1;
		stream1 << "Exception occurred while uploading: " << e.what();
		errMsg = stream1.str();
	}
	
	return "";
}
