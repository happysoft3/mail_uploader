
#include "parameterLoader.h"
#include "common/utils/stringhelper.h"
#include "common/utils/pathManager.h"
#include "common/utils/myDebug.h"

using namespace _StringHelper;
constexpr char upload_ini_mainkey[] = "UPLOADING";

ParameterLoader::ParameterLoader()
	: IniFileMan()
{ }

ParameterLoader::~ParameterLoader()
{ }

void ParameterLoader::catchException(const std::exception &except)
{
	throw except;
}

std::unique_ptr<PathManager> ParameterLoader::loadBasepath()
{
	std::string basePath;

	GetItemValue(upload_ini_mainkey, "BASEPATH", basePath);
	if (basePath.empty())
		throw std::logic_error("has no attrib 'basepath'");

	auto pathman = std::make_unique<PathManager>();

	pathman->SetUrl(basePath);
	return pathman;
}

constexpr char upload_file_key_format[] = "UPLOAD_FILE%d";
void ParameterLoader::loadFileList()
{
	int counter = 1;
	std::string source = stringFormat(upload_file_key_format, counter);
	std::string value;

	while (GetItemValue(upload_ini_mainkey, source, value))
	{
		m_fileList.push_back(value);
		source = stringFormat(upload_file_key_format, ++counter);
	}
	if (m_fileList.empty())
		throw std::logic_error("has no attribution 'source'");
}

void ParameterLoader::loadServerUrl()
{
	std::string servUrl;

	if (!GetItemValue(upload_ini_mainkey, "SERVER_URL", servUrl))
		throw std::logic_error("server url is missing");

	if (servUrl.empty())
		throw std::logic_error("has no attrib 'server url'");

	m_serverUrl = std::move(servUrl);
}

/*
이런식->
/dwp/com/portal/main.nsf/wfrmpage?ReadForm&url=/mail/hq/1/P00001.nsf/wCreDoc?openagent&inherit=
그러나 여기에서 맨 처음 = 다음부터 .nsf 까지를 추출해야 합니다
결과적으로, 따라오는 경로가 개인 메일 경로입니다
/mail/hq/1/P00001.nsf
*/

void ParameterLoader::fetchUserMailPath(const std::string &t, std::string &mailPath)
{
	constexpr auto symb_entry_spot = '=';
	constexpr auto symb_end_spot = '?';
	constexpr auto null_spot = std::string::npos;
	
	size_t pos = 0, validEntrySpot = null_spot;
	size_t lastSlash = null_spot;

	for (const auto &c : t)
	{
		switch (c)
		{
		case symb_entry_spot:
			if (validEntrySpot == null_spot)
				validEntrySpot = pos;
			break;

		case '/':
			if (validEntrySpot != null_spot)
				lastSlash = pos;
			break;

		case symb_end_spot:
			if (validEntrySpot != null_spot)
			{
				if (lastSlash == null_spot)
					MY_THROW() << "no slash";
				mailPath = t.substr(validEntrySpot + 1, lastSlash - validEntrySpot);
				return;
			}
		}
		++pos;
	}
	if (!validEntrySpot)
		MY_THROW() << "no entry: " << symb_entry_spot;
	MY_THROW() << "no end spot: " << symb_end_spot;
}

void ParameterLoader::verySimpleParseOpenUrl(const std::string &t)
{
	fetchUserMailPath(t, m_userMailPath);
}

void ParameterLoader::loadUserMailPath()
{
	std::string openUrl;

	if (!GetItemValue(upload_ini_mainkey, "OPEN_URL", openUrl))
		return;

	verySimpleParseOpenUrl(openUrl);

	GetItemValue(upload_ini_mainkey, "USER_AUTH", m_userAuthData);
}

const char *const ParameterLoader::ServerURL() const
{
	return m_serverUrl.empty() ? nullptr : m_serverUrl.c_str();
}

void ParameterLoader::PreorderFileList(std::function<void(const std::string &, int)> &&fn, bool full)
{
	int index = 0;

	for (const auto &s : m_fileList)
		fn(full ? m_basepathMan->FullPath(s) : s, index++);
}

bool ParameterLoader::ReadIni(const std::string &inifile)
{
	try
	{
		IniFileMan::ReadIni(inifile);
		m_basepathMan = loadBasepath();
		loadFileList();
		loadServerUrl();
		loadUserMailPath();
	}
	catch (const std::logic_error &except)
	{
		std::cout << stringFormat("%s::%s", __FUNCDNAME__, except.what());
		return false;
	}
	return true;
}

std::unique_ptr<PathManager> ParameterLoader::CreatePath(const std::string &subPath) const
{
	auto ret = std::make_unique<PathManager>();

	ret->SetUrl(m_basepathMan->FullPath());
	ret->SetSubName(subPath);
	return ret;
}

