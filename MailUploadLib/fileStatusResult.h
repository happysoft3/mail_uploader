
#ifndef FILE_STATUS_RESULT_H__
#define FILE_STATUS_RESULT_H__

#include <map>
#include <string>

class TaskUnit;
class TaskResult;

class FileStatusResult
{
private:
	std::map<std::string, int> m_taskIdMap;
	std::map<int, std::string> m_statusMap;

public:
	FileStatusResult();
	~FileStatusResult();

private:
	bool getIDFromTask(const TaskResult &result, int &id);

public:
	void PutID(const TaskUnit &task, int id);
	void PutStatus(const TaskResult &result, const std::string &status);
	const char *const GetStatus(int id) const;
};

#endif

