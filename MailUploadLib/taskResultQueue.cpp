
#include "taskResultQueue.h"
#include "abstractTaskResult.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

TaskResultQueue::TaskResultQueue()
{ }

TaskResultQueue::~TaskResultQueue()
{ }

void TaskResultQueue::Push(std::unique_ptr<AbstractTaskResult> result)
{
	if (!result)
		return;

	auto uniqueKey = result->UniqueID();

	if (m_resultMap.find(uniqueKey) != m_resultMap.cend())
		throw std::logic_error(stringFormat("conflict result key '%s'", uniqueKey));

	m_resultQueue.push_back(std::move(result));
	m_resultMap[uniqueKey] = std::prev(m_resultQueue.end());
}

std::unique_ptr<AbstractTaskResult> TaskResultQueue::Pop()
{
	if (m_resultQueue.empty())
		return {};

	std::unique_ptr<AbstractTaskResult> task = std::move(m_resultQueue.front());

	m_resultMap.erase(task->UniqueID());
	m_resultQueue.pop_front();
	return task;
}

