
#include "fileStatusResult.h"
#include "common/task/taskUnit.h"
#include "common/task/taskResult.h"

FileStatusResult::FileStatusResult()
{ }

FileStatusResult::~FileStatusResult()
{
}

bool FileStatusResult::getIDFromTask(const TaskResult &result, int &id)
{
	auto idIterator = m_taskIdMap.find(result.TaskID());

	if (idIterator == m_taskIdMap.cend())
		return false;

	id = idIterator->second;
	return true;
}

void FileStatusResult::PutID(const TaskUnit &task, int id)
{
	std::string uniqKey = task.Index();

	if (uniqKey.empty())
		return;

	m_taskIdMap[uniqKey] = id;
}

void FileStatusResult::PutStatus(const TaskResult &result, const std::string &status)
{
	int id;

	if (!getIDFromTask(result, id))
		return;

	m_statusMap[id] = status;
}

const char *const FileStatusResult::GetStatus(int id) const
{
	auto statIterator = m_statusMap.find(id);

	if (statIterator == m_statusMap.cend())
		return "empty";

	return statIterator->second.c_str();
}
