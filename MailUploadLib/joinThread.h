
#ifndef JOIN_THREAD_H__
#define JOIN_THREAD_H__

#include "taskThread.h"
#include <vector>

class AbstractTaskResult;

class JoinThread : public TaskThread
{
private:
	std::vector<std::unique_ptr<TaskThread>> m_workerVector;
	std::unique_ptr<TaskResultQueue> m_wholeResultQueue;

public:
	JoinThread();
	~JoinThread() override;

private:
	bool beforeTasking() override;
	void accumulateTaskResult(TaskThread &worker);
	std::unique_ptr<AbstractTaskResult> tryGetResult(bool &keep) override;

public:
	void AttachWorkerVector(std::vector<std::unique_ptr<TaskThread>> &&workers);
};

#endif

