
#ifndef ABSTRACT_TASK_RESULT_H__
#define ABSTRACT_TASK_RESULT_H__

#include <string>
#include <map>

class AbstractTaskResult
{
private:
	std::string m_uniqueId;
	std::map<std::string, std::string> m_storedData;

public:
	AbstractTaskResult();
	virtual ~AbstractTaskResult();

private:
	static std::string publishUniqueId();

public:
	void SetData(const std::string &key, const std::string &value);
	const char *const GetData(const std::string &key) const;
	const char *const UniqueID() const
	{
		return m_uniqueId.c_str();
	}
};

#endif

