
#include "uploadTask.h"
#include "uploadResult.h"
#include "parameterLoader.h"
#include "impl/netUploadFile.h"
#include "impl/netSendPostMsg.h"
#include "common/utils/pathManager.h"
#include "common/utils/randomString.h"
#include "common/utils/myDebug.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

UploadTask::UploadTask(std::weak_ptr<TaskUnit::Controller> controller)
	: TaskUnit(controller)
{ }

UploadTask::~UploadTask()
{ }

void UploadTask::checkExpired()
{
	if (isExpired())
		MY_THROW() << "task is cancelled";
}

std::unique_ptr<NetUploadFile> UploadTask::makeUploaderObject()
{
	if (m_serverFilename.empty())
		MY_THROW() << "server file is empty";

	auto uploader = std::make_unique<NetUploadFile>();

	uploader->PutServerUrl(m_serverUrl);
	uploader->PutServerFile(stringFormat("%s.eml", m_serverFilename));
	return uploader;
}

void UploadTask::detailRunEx(NetUploadFile &uploader)
{
	m_result->SetLocalFileURL(m_filePath->SubName());
	uploader.PutLocalFile(m_filePath->FullPath());

	if (!uploader.Run())
		MY_THROW() << uploader.DetailError();
}

void UploadTask::afterRun(NetUploadFile &uploader)
{
	std::string rootFolder = uploader.RootFolder();

	if (rootFolder.empty())
		MY_THROW() << "maybe fail to upload";

	m_result->SetServerFileURL(rootFolder);

	auto postSend = std::make_unique<NetSendPostMsg>(uploader);

	postSend->PutUploadedParam(rootFolder, uploader.ServerFile());
	postSend->PutUserMailPath(m_userMailPath);
	postSend->PutCookie(m_cookie);
	if (!postSend->Run())
		MY_THROW() << postSend->DetailError();
}

void UploadTask::Run()
{
	m_result = std::make_unique<UploadResult>(*this);
	
	try
	{
		checkExpired();
		m_OnTaskStart.Emit(m_filePath->SubName());
		auto uploader = makeUploaderObject();
		
		detailRunEx(*uploader);
		checkExpired();
		afterRun(*uploader);
	}
	catch (std::exception &e)
	{
		m_result->PutError(e);
	}
}

std::unique_ptr<TaskResult> UploadTask::GetResult()
{
	if (m_result)
	{
		std::shared_ptr<UploadResultData> clone = m_result->CloneData();

		m_OnEndedTask.Emit(clone);
		return std::move(m_result);
	}
	return {};
}

void UploadTask::PutParams(ParameterLoader &param, const std::string &fullPath)
{
	m_filePath = std::make_unique<PathManager>();
	m_filePath->SetUrl(fullPath);
	m_serverUrl = param.ServerURL();
	m_serverFilename = RandomString::Generate(16);
	m_userMailPath = param.UserMailPath();
	m_cookie = param.UserAuthData();
}

