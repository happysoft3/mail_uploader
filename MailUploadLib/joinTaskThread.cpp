
#include "joinTaskThread.h"
#include "uploadThread.h"
#include "common/task/taskResultHash.h"
#include "common/task/taskQueue.h"

static constexpr size_t numof_thread = 4;

JoinTaskThread::JoinTaskThread()
	: TaskThread()
{
	createNumofThread(numof_thread);
}

JoinTaskThread::~JoinTaskThread()
{ }

void JoinTaskThread::createNumofThread(size_t n)
{
	m_procVector.resize(n);
	for (auto &proc : m_procVector)
		proc = std::make_unique<UploadThread>();
}

bool JoinTaskThread::beforeExec(std::shared_ptr<TaskQueue> queue)
{
	for (auto &proc : m_procVector)
		proc->Perform(queue);

	return false;
}

void JoinTaskThread::afterExec(TaskResultHash &resultMap) noexcept
{
	for (auto &proc : m_procVector)
	{
		auto procErr = proc->GetError();

		if (procErr)
		{
			PutError(*procErr);
			return;
		}
		auto res = proc->GetResult();

		if (!res->Empty())
			resultMap.Merge(*res);
	}
	m_OnJoinFinish.Emit();
}
