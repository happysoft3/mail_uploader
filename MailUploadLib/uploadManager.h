
#ifndef UPLOAD_MANAGER_H__
#define UPLOAD_MANAGER_H__

#include "common/ccobject/ccobject.h"
#include <string>
#include <memory>
#include <list>
#include <sstream>
#include <atomic>

class ParameterLoader;
class UploadTask;
class TaskQueue;
class JoinTaskThread;
class TaskResult;
struct UploadResultData;
class FileStatusResult;

class UploadManager : public CCObject
{
	struct Core;
private:
	std::unique_ptr<TaskQueue> m_taskQueue;
	std::unique_ptr<ParameterLoader> m_paramLoader;
	std::unique_ptr<JoinTaskThread> m_joinThread;
	std::shared_ptr<bool> m_masterKey;
	std::atomic<int> m_progress;
	std::atomic<int> m_errorTaskCount;
	std::unique_ptr<Core> m_core;
	std::unique_ptr<FileStatusResult> m_procResult;

public:
	UploadManager();
	~UploadManager() override;

private:
	void createUploadTask(const std::string &fullPath, int index);
	void onTaskStart(const std::string &fn);
	void onTaskEnd(std::shared_ptr<UploadResultData> res);
	void onEndJoinThread();

	template <class Arg>
	void logPrintOutImpl(std::stringstream &ss, Arg &&arg)
	{
		ss << arg;
	}

	template <class Arg, class... Args>
	void logPrintOutImpl(std::stringstream &ss, Arg &&arg, Args&&... args)
	{
		ss << arg;
		logPrintOutImpl(ss, std::forward<Args>(args)...);
	}

	template <class... Args>
	void logPrintOut(Args&&... args)
	{
		std::stringstream ss;

		logPrintOutImpl(ss, std::forward<Args>(args)...);
		//m_OnPrintLog.Emit(ss.str());
		m_OnPrintLog.QueueEmit(ss.str());
	}

public:
	//업로드 하기 전, 준비작업을 여기에서 수행합니다
	void PrematurePreparation(std::unique_ptr<ParameterLoader> paramLoader);
	void DoUpload();

private:
	void computeErrorCount(TaskResult &result, int &errorCount);

public:
	std::unique_ptr<FileStatusResult> CheckResult();
	void CancelUpload();

	DECLARE_SIGNAL(OnPrintLog, std::string)
	DECLARE_SIGNAL(OnReportProgress, int)
	DECLARE_SIGNAL(OnReportEndProgress)
};

#endif

