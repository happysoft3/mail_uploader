
#include "taskThreadManager.h"
#include "uploadThread.h"
#include "joinThread.h"
#include "taskQueue.h"
#include "abstractTaskResult.h"
#include <iostream>

TaskThreadManager::TaskThreadManager()
	: AbstractTask()
{ }

TaskThreadManager::~TaskThreadManager()
{ }

void TaskThreadManager::perform()
{
	if (!m_queue)
		throw std::logic_error("has no attrib 'queue'");
	m_queue.reset();
	if (m_threadVector.empty())
		throw std::logic_error("no thread");

	for (auto &worker : m_threadVector)
		remotePerform(*worker);
}

void TaskThreadManager::testOnOverOneStep(AbstractTaskResult &res)
{
	std::cout << "end task" << res.UniqueID() << std::endl;
}

void TaskThreadManager::testOnOverThread()
{
	std::cout << "end thread" << std::endl;
}

void TaskThreadManager::Create(std::unique_ptr<TaskQueue> queue, std::shared_ptr<bool> key)
{
	std::shared_ptr<TaskQueue> sharedQueue = std::move(queue);

	m_threadVector.resize(3);
	for (auto &worker : m_threadVector)
	{
		auto up = std::make_unique<UploadThread>();

		up->PutSharedQueue(sharedQueue);
		worker = std::move(up);
		worker->SetKey(key);
		worker->RegistCallbackOneStep([this](AbstractTaskResult &res) { testOnOverOneStep(res); });
		worker->RegistCallbackShutdown([this]() { testOnOverThread(); });
	}
	m_queue = std::move(sharedQueue);
}

std::unique_ptr<std::exception> TaskThreadManager::GetError()
{
	return m_error ? std::move(m_error) : nullptr;
}

bool TaskThreadManager::DoProcess()
{
	try
	{
		perform();
		auto joinThread = std::make_unique<JoinThread>();

		joinThread->AttachWorkerVector(std::move(m_threadVector));
		m_joinThread = std::move(joinThread);		
	}
	catch (const std::exception &except)
	{
		m_error = std::make_unique<std::exception>(except);
		return false;
	}
	remotePerform(*m_joinThread);

	return true;
}

