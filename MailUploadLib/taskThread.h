
#ifndef TASK_THREAD_H__
#define TASK_THREAD_H__

#include "abstractTask.h"
#include <memory>
#include <future>
#include <functional>

class AbstractTaskResult;
class TaskResultQueue;

class TaskThread : public AbstractTask
{
private:
	std::shared_ptr<bool> m_enableKey;
	std::shared_ptr<bool> m_restrictKey;

	using task_result_ty = std::unique_ptr<TaskResultQueue>;
	std::unique_ptr<std::future<task_result_ty>> m_archive;

	using task_thread_cb_ty = std::function<void(AbstractTaskResult&)>;
	std::shared_ptr<task_thread_cb_ty> m_cbOneStep;	//작업 하나 끝났을 때
	std::shared_ptr<std::function<void()>> m_cbShutdown;	//스레드가 완주했을 때
	
public:
	TaskThread();
	~TaskThread() override;

private:
	virtual bool beforeTasking() { return true; }
	virtual std::unique_ptr<AbstractTaskResult> tryGetResult(bool &keep);
	void onWorkerShutdown();
	std::unique_ptr<TaskResultQueue> doTask(std::weak_ptr<bool> privateKey);
	void perform() override;

public:
	void SetKey(std::shared_ptr<bool> &key);
	std::unique_ptr<TaskResultQueue> FetchResult();
	void RegistCallbackOneStep(task_thread_cb_ty &&cb);
	void RegistCallbackShutdown(std::function<void()> &&cb);
};

#endif

