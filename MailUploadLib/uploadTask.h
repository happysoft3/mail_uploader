
#ifndef UPLOAD_TASK_H__
#define UPLOAD_TASK_H__

#include "common/task/taskUnit.h"
#include <string>

class PathManager;
class ParameterLoader;
class UploadResult;
struct UploadResultData;

class NetUploadFile;

class UploadTask : public TaskUnit
{
private:
	std::string m_serverUrl;
	std::unique_ptr<PathManager> m_filePath;
	std::unique_ptr<UploadResult> m_result;
	std::string m_serverFilename;
	std::string m_userMailPath;
	std::string m_cookie;

public:
	UploadTask(std::weak_ptr<TaskUnit::Controller> controller);
	~UploadTask() override;

private:
	void checkExpired();
	std::unique_ptr<NetUploadFile> makeUploaderObject();
	void detailRunEx(NetUploadFile &pocoRunner);
	void afterRun(NetUploadFile &poco);
	void Run() override;
	std::unique_ptr<TaskResult> GetResult() override;

public:
	void PutParams(ParameterLoader &param, const std::string &fullPath);

	DECLARE_SIGNAL(OnTaskStart, const std::string)
	DECLARE_SIGNAL(OnEndedTask, std::shared_ptr<UploadResultData>)
};

#endif

