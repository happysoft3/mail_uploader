
#include "netUploadFile.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"
#include "common/include/incFilesystem.h"
#include "common/utils/stringhelper.h"
#include "json/json.h"
#include <fstream>

using namespace _StringHelper;

struct NetUploadFile::UploadFile
{
	std::string m_name;
	uint64_t m_size;
	std::ifstream m_file;
};

class NetUploadFile::SendData
{
public:
	enum class Priority
	{
		FIRST,
		SECOND,
	};
private:
	std::string m_data;

public:
	SendData(const std::string &s)
		: m_data(s)
	{ }
	virtual ~SendData()
	{ }

	virtual Priority Order() const
	{
		return Priority::FIRST;
	}

	const char *const Get() const
	{
		return m_data.c_str();
	}
};

class PostData : public NetUploadFile::SendData
{
private:
	Priority Order() const override
	{
		return Priority::SECOND;
	}

public:
	PostData(const std::string &s)
		: SendData(s)
	{ }
};

NetUploadFile::NetUploadFile()
	: PocoNetClientObject()
{ }

NetUploadFile::~NetUploadFile()
{ }

void NetUploadFile::pushSendData(const std::string &data, bool post)
{
	m_sendData.push_back(post ? std::make_unique<PostData>(data) : std::make_unique<SendData>(data));
	m_dataLength += data.length();
}

void NetUploadFile::onPreSendRequest(Poco::Net::HTTPRequest &req)
{
	std::ifstream file(m_upFile->m_name, std::ios::binary);

	if (!file)
		throw std::logic_error(stringFormat("cannot open the file %s", m_upFile->m_name));

	m_upFile->m_file = std::move(file);

	req.setContentType("multipart/form-data; boundary=-------------------------87142694621188");
	pushSendData("---------------------------87142694621188\r\nContent-Disposition: form-data; name=\"data\"; filename=\"");
	pushSendData(m_servFile);
	pushSendData("\";\r\nContent-Type: application/octet-stream\r\n\r\n");
	pushSendData("\r\n---------------------------87142694621188--\r\n", true);

	req.setContentLength(m_dataLength + m_upFile->m_size);
}

void NetUploadFile::onSendRequest(std::ostream &os)
{
	setRequestContentLength(m_dataLength + m_upFile->m_size);
	for (const auto &d : m_sendData)
	{
		if (d->Order() == SendData::Priority::FIRST)
			os << d->Get();
	}
	os << m_upFile->m_file.rdbuf();
	for (const auto &d : m_sendData)
	{
		if (d->Order() == SendData::Priority::SECOND)
			os << d->Get();
	}
	m_sendData.clear();
}

void NetUploadFile::onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw)
{
	using namespace Poco::Net;
	//Get status code
	int statusCode = (int)resp.getStatus();

	//Get status
	std::string status = resp.getReason();

	if (statusCode == Poco::Net::HTTPResponse::HTTP_OK)
	{
		Json::Reader reader;
		Json::Value root;

		if (reader.parse(raw, root))
			m_rootFolder = root["folder"].asString();
		return;
	}
	throw std::logic_error(stringFormat("resp code: %d, reason: %s", statusCode, status));
}

void NetUploadFile::runClient()
{
	using namespace Poco::Net;
	
	sendRequest(HTTPRequest::HTTP_POST, HTTPMessage::HTTP_1_1);
	receiveResponse();
}

void NetUploadFile::PutLocalFile(const std::string &fn)
{
	auto pth = NAMESPACE_FILESYSTEM::path(fn);

	if (!NAMESPACE_FILESYSTEM::exists(pth))
		throw std::logic_error(stringFormat("cannot find file %s", fn));

	m_upFile = std::make_unique<UploadFile>();
	m_upFile->m_name = pth.string();
	m_upFile->m_size = NAMESPACE_FILESYSTEM::file_size(pth);
}


