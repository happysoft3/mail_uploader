
#ifndef NET_UPLOAD_FILE_H__
#define NET_UPLOAD_FILE_H__

#include "common/net/pocoNetClientObject.h"
#include <list>

class NetUploadFile : public PocoNetClientObject
{
	struct UploadFile;

public:
	class SendData;
private:
	std::list<std::unique_ptr<SendData>> m_sendData;
	uint64_t m_dataLength;
	std::unique_ptr<UploadFile> m_upFile;
	std::string m_rootFolder;
	std::string m_servFile;

public:
	NetUploadFile();
	~NetUploadFile() override;

private:
	void pushSendData(const std::string &data, bool post = false);
	void onPreSendRequest(Poco::Net::HTTPRequest &req) override;
	void onSendRequest(std::ostream &os) override;
	void onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw) override;
	void runClient() override;

public:
	const char *const RootFolder() const
	{
		return m_rootFolder.empty() ? "" : m_rootFolder.c_str();
	}
	const char *const ServerFile() const
	{
		return m_servFile.empty() ? "" : m_servFile.c_str();
	}
	void PutServerFile(const std::string &fn)
	{
		m_servFile = fn;
	}
	void PutLocalFile(const std::string &fn);
};

#endif

