
#ifndef NET_SEND_POST_MSG_H__
#define NET_SEND_POST_MSG_H__

#include "common/net/pocoNetClientObject.h"

class NetSendPostMsg : public PocoNetClientObject
{
private:
	std::string m_userMailPath;
	std::string m_queryFolder;
	std::string m_queryFilename;
	std::string m_cookie;

public:
	NetSendPostMsg(PocoNetClientObject &other);
	~NetSendPostMsg() override;

private:
	void settingRequestUri(Poco::URI &uri) override;
	void onPreSendRequest(Poco::Net::HTTPRequest &req) override;
	void onSendRequest(std::ostream &os) override;
	void onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw) override;
	void runClient() override;

public:
	void PutUserMailPath(const std::string &mailPath)
	{
		m_userMailPath = mailPath;
	}
	void PutCookie(const std::string &ck);
	void PutUploadedParam(const std::string &folder, const std::string &fn);
};

#endif

