
#include "netSendPostMsg.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"
#include "common/utils/stringhelper.h"

using namespace _StringHelper;

NetSendPostMsg::NetSendPostMsg(PocoNetClientObject &other)
	: PocoNetClientObject(other)
{ }

NetSendPostMsg::~NetSendPostMsg()
{ }

void NetSendPostMsg::settingRequestUri(Poco::URI &uri)
{
	using namespace Poco;
	uri.setPath(m_userMailPath + "wUpDoc");

	uri.setRawQuery("OpenAgent");
	uri.addQueryParameter("sfolder", m_queryFolder);
	uri.addQueryParameter("filename", m_queryFilename);
}

void NetSendPostMsg::onPreSendRequest(Poco::Net::HTTPRequest &req)
{
	req.setContentType("text/plain");
	req.setContentLength(static_cast<uint64_t>(0));
	if (m_cookie.size())
		req.set("Cookie", m_cookie);
}

void NetSendPostMsg::onSendRequest(std::ostream &os)
{
	return;
}

void NetSendPostMsg::onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw)
{
	using namespace Poco::Net;
	//Get status code
	int statusCode = (int)resp.getStatus();

	//Get status
	std::string status = resp.getReason();

	if (statusCode != Poco::Net::HTTPResponse::HTTP_OK)
		throw std::logic_error(stringFormat("resp code: %d, reason: %s", statusCode, status));
}

void NetSendPostMsg::runClient()
{
	using namespace Poco::Net;

	sendRequest(HTTPRequest::HTTP_GET, HTTPMessage::HTTP_1_1);
	receiveResponse();
}

void NetSendPostMsg::PutCookie(const std::string &ck)
{
	if (ck.empty())
		return;

	m_cookie = ck.front() == '=' ? ck.substr(1) : ck;
}

void NetSendPostMsg::PutUploadedParam(const std::string &folder, const std::string &fn)
{
	m_queryFilename = fn;
	m_queryFolder = folder;
}
