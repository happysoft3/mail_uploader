
#ifndef TASK_QUEUE_H__
#define TASK_QUEUE_H__

#include <list>
#include <memory>
#include <mutex>

class AbstractTask;

class TaskQueue
{
private:
	std::list<std::unique_ptr<AbstractTask>> m_taskList;

public:
	TaskQueue();
	~TaskQueue();

	void Push(std::unique_ptr<AbstractTask> task);
	std::unique_ptr<AbstractTask> Pop();

private:
	std::mutex m_lock;
};

#endif

