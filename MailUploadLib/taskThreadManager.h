
#ifndef TASK_THREAD_MANAGER_H__
#define TASK_THREAD_MANAGER_H__

#include "abstractTask.h"
#include <vector>
#include <memory>

class TaskQueue;
class TaskThread;

class TaskThreadManager : public AbstractTask
{
private:
	std::vector<std::unique_ptr<TaskThread>> m_threadVector;
	std::unique_ptr<std::exception> m_error;
	std::shared_ptr<TaskQueue> m_queue;
	std::unique_ptr<TaskThread> m_joinThread;

public:
	TaskThreadManager();
	~TaskThreadManager();

private:
	void perform() override;
	void testOnOverOneStep(AbstractTaskResult &res);
	void testOnOverThread();

public:
	void Create(std::unique_ptr<TaskQueue> queue, std::shared_ptr<bool> key = nullptr);
	std::unique_ptr<std::exception> GetError();
	bool DoProcess();
};

#endif

