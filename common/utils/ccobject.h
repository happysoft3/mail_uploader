
#ifndef CCOBJECT_H__
#define CCOBJECT_H__

#include <memory>
#include <future>
#include <mutex>
#include <list>

class CCObject
{
protected:
	class Core;
	std::shared_ptr<Core> m_core;
	std::weak_ptr<Core> m_parent;
	std::unique_ptr<std::future<bool>> m_allowDeleter;

public:
	CCObject(CCObject *parent = nullptr);
	virtual ~CCObject();
};

namespace _event_signal_slot_private
{
	template <class... Args>
	class EventSlot;
}

template <class... Args>
class _event_signal_slot_private::EventSlot : public CCObject
{
	using slot_functionType = std::function<void(Args...)>;
private:
	slot_functionType m_slot;
	std::weak_ptr<CCObject::Core> m_refTarget;

public:
	explicit EventSlot(slot_functionType &&slot, std::shared_ptr<CCObject::Core> target)
		: CCObject(),
		m_slot(std::forward<slot_functionType>(slot))
	{
		m_refTarget = target;
	}

	~EventSlot() override
	{ }

	template <class... SlotArgs>
	void Emit(SlotArgs&&... args)
	{
		m_slot(std::forward<SlotArgs>(args)...);
	}

	inline bool Expired() const
	{
		return m_refTarget.expired();
	}
};

template <class... Args>
class EventSignal : public CCObject
{
	using slot_type = _event_signal_slot_private::EventSlot<Args...>;
	using slot_container = std::list<std::shared_ptr<slot_type>>;
	using slot_iterator = typename slot_container::iterator;

private:
	slot_container m_slots;

public:
	explicit EventSignal()
		: CCObject()
	{ }

	~EventSignal() override
	{ }

private:
	template <class SlotFunction>
	void addSlot(SlotFunction &&slot, CCObject *pRecv)
	{
		auto slotObject = std::shared_ptr<slot_type>(new slot_type(std::forward<SlotFunction>(slot), (*pRecv)()));

		m_slots.push_back(std::move(slotObject));
	}

public:
	template <class SlotFunction, class RecvInstance>
	bool Connection(SlotFunction &&slot, RecvInstance *pRecv)
	{
		static_assert(std::is_base_of<CCObject, RecvInstance>::value, "the instance must inherit CCObject");
		if (pRecv == nullptr)
			return false;

		std::weak_ptr<CCObject::Core> refptr = (*static_cast<CCObject *>(pRecv))();
		std::lock_guard<std::mutex> lock(m_lock);
		addSlot([callable = std::move(slot), refpt = std::move(refptr)](Args&&... args)
		{
			std::shared_ptr<CCObject::Core> recv = refpt.lock();

			if (!recv)
				return;

			(static_cast<RecvInstance *>((*recv)())->*callable)(std::forward<Args>(args)...);
		}, pRecv);
		return true;
	}

	template <class... SlotArgs>
	void Emit(SlotArgs&&... args)
	{
		std::list<slot_iterator> removeSlotList;
		{
			std::lock_guard<std::mutex> lock(m_lock);
			slot_iterator travsCur = m_slots.begin();

			while (travsCur != m_slots.end())
			{
				if ((*travsCur)->Expired())
					removeSlotList.push_back(travsCur);
				else
					(*travsCur)->Emit(std::forward<SlotArgs>(args)...);
				++travsCur;
			}

			for (const auto &iter : removeSlotList)
				m_slots.erase(iter);
		}
	}

private:
	std::mutex m_lock;
};

#endif

