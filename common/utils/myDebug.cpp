
#include "myDebug.h"
#include "stringhelper.h"
#include <iostream>

using namespace _StringHelper;

MyDebug::MyDebug(const std::string &pos)
	: CFlush::Context()
{
	m_triggered = std::make_unique<bool>(false);
	m_pos = pos;
}

MyDebug::~MyDebug()
{
}

void MyDebug::Put(const std::string &s)
{
	if (!m_triggered)
		return;

	m_triggered.reset();
	throw std::logic_error(stringFormat("code: %s : %s", m_pos, s));
}

MyThrow::MyThrow(CFlush::Context *ctx)
{
	m_flush = std::make_unique<CFlush>(ctx);
}
MyThrow::~MyThrow() noexcept(false)
{
	m_flush->Flush();
	m_flush.reset();
}

MyCOut::MyCOut()
{
}

MyCOut::~MyCOut()
{
}

void MyCOut::Put(const std::string &s)
{
	std::cout << " > " << s << std::endl;
}
