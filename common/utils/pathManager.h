
#ifndef PATH_MANAGER_H__
#define PATH_MANAGER_H__

#include <string>

class PathManager
{
private:
    std::string m_targetUrl;
	std::string m_subTerm;

public:
    PathManager();
    ~PathManager();

	const char *const SubName() const
	{
		return m_subTerm.c_str();
	}
	void SetSubName(const std::string &subName);
    void SetUrl(const std::string &url);
    const char *BasePath() const;

private:
	std::string loadTarget() const;

public:
    std::string FullPath(const std::string &subUrl, bool encUtf8 = false) const;
	std::string FullPath(bool encUtf8 = false) const;
};

#endif

