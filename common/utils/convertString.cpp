
#include "convertString.h"
#include <windows.h>

bool ConvertString::AnsiToUnicode(const std::string &src, std::wstring &dest)
{
	if (src.empty())
		return false;

	if (dest.length())
		dest.clear();
	uint32_t destlength = ::MultiByteToWideChar(CP_ACP, 0, src.c_str(), src.size(), nullptr, 0);
	dest.resize(destlength);

	return ::MultiByteToWideChar(CP_ACP, 0, src.c_str(), src.size(), const_cast<wchar_t *>(dest.c_str()), dest.size()) > 0;
}

bool ConvertString::UnicodeToAnsi(const std::wstring &src, std::string &dest)
{
	if (!src.empty())
	{
		dest.clear();
		uint32_t commitLength = ::WideCharToMultiByte(CP_ACP, 0, const_cast<wchar_t*>(src.c_str()), src.size(), nullptr, 0, nullptr, nullptr);
		dest.resize(commitLength);
		::WideCharToMultiByte(CP_ACP, 0, const_cast<wchar_t*>(src.c_str()), src.size(), const_cast<char*>(dest.c_str()), commitLength, nullptr, nullptr);
		return true;
	}
	return false;
}

bool ConvertString::UnicodeToUtf8(std::wstring &src, std::string &dest)
{
	if (!src.empty())
	{
		uint32_t commitLength = ::WideCharToMultiByte(CP_UTF8, 0, &src[0], src.length(), nullptr, 0, nullptr, nullptr);
		dest.resize(commitLength);
		::WideCharToMultiByte(CP_UTF8, 0, &src[0], src.length(), &dest[0], commitLength, nullptr, nullptr);
		return true;
	}
	return false;
}

bool ConvertString::Utf8ToUnicode(std::string &src, std::wstring &dest)
{
	if (!src.empty())
	{
		uint32_t commitLength = ::MultiByteToWideChar(CP_UTF8, 0, &src[0], src.size(), nullptr, 0);
		dest.resize(commitLength, 0);
		::MultiByteToWideChar(CP_UTF8, 0, &src[0], src.size(), &dest[0], commitLength);
		return true;
	}
	return false;
}
