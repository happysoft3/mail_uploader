
#include "pathManager.h"
#include "stringhelper.h"
#include "../include/incFilesystem.h"

using namespace _StringHelper;
static auto s_slashConvert = [](const char &c) { return (c == '/') ? '\\' : c; };

PathManager::PathManager()
{ }

PathManager::~PathManager()
{ }

void PathManager::SetSubName(const std::string &subName)
{
	if (subName.empty())
		return;

	m_subTerm.resize(subName.size());
	std::transform(subName.cbegin(), subName.cend(), m_subTerm.begin(), s_slashConvert);
}

void PathManager::SetUrl(const std::string &url)
{
    if (url.empty())
    {
        if (m_targetUrl.size())
            m_targetUrl.clear();
        return;
    }

    m_targetUrl.resize(url.size());
    std::transform(url.cbegin(), url.cend(), m_targetUrl.begin(), s_slashConvert);
    size_t lastSlash = url.find_last_of('\\');

	if (lastSlash != std::string::npos)
	{
		m_subTerm = url.substr(lastSlash + 1);
		m_targetUrl.resize(lastSlash);
	}
}

const char *PathManager::BasePath() const
{
    if (m_targetUrl.empty())
        return "";

    return m_targetUrl.c_str();
}

std::string PathManager::loadTarget() const
{
	if (m_targetUrl.empty())
		return {};

	if (m_targetUrl.back() == '\\')
		return m_targetUrl;

	return m_targetUrl + '\\';
}

std::string PathManager::FullPath(const std::string &subUrl, bool encUtf8) const
{
	std::string ret;

	if (m_targetUrl.empty())
		ret = subUrl;
	else if (subUrl.empty())
		ret = loadTarget();
	else
		ret = stringFormat("%s%s", loadTarget(), subUrl);

	if (encUtf8)
		return NAMESPACE_FILESYSTEM::path(ret).u8string();
	return ret;
}

std::string PathManager::FullPath(bool encUtf8) const
{
	return FullPath(m_subTerm, encUtf8);
}
