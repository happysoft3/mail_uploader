
#ifndef RANDOM_STRING_H__
#define RANDOM_STRING_H__

#include <string>

class RandomString
{
private:
	RandomString();
	~RandomString();
	RandomString(const RandomString &) = delete;
	RandomString(RandomString &&) = delete;
	RandomString &operator=(RandomString &&) = delete;
	RandomString &operator=(const RandomString &) = delete;

public:
	static void Generate(std::string &s);
	static std::string Generate(size_t length);
};

#endif

