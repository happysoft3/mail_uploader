
#ifndef MY_DEBUG_H__
#define MY_DEBUG_H__

#include "cflush.h"
#include <string>

class MyDebug : public CFlush::Context
{
private:
	std::unique_ptr<bool> m_triggered;
	std::string m_pos;

public:
	MyDebug(const std::string &pos);
	~MyDebug() override;

private:
	void Put(const std::string &s) override;
};

class MyThrow
{
private:
	std::unique_ptr<CFlush> m_flush;

public:
	MyThrow(CFlush::Context *ctx);
	~MyThrow() noexcept(false);

	template <class T>
	CFlush &operator<<(const T &n)
	{
		(*m_flush) << n;
		return *m_flush;
	}
};

class MyCOut : public CFlush::Context
{
public:
	MyCOut();
	~MyCOut() override;

private:
	void Put(const std::string &s) override;
};

#define _MY_DEBUG_TOSTR(s) #s
#define MY_THROW() MyThrow(&MyDebug(__FILE__ ":" _MY_DEBUG_TOSTR(__LINE__)))
#define MY_PRINT() CFlush(&MyCOut())

#endif
