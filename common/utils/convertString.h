
#ifndef CONVERT_STRING_H__
#define CONVERT_STRING_H__

#include <string>

namespace ConvertString
{
	bool AnsiToUnicode(const std::string &src, std::wstring &dest);
	bool UnicodeToAnsi(const std::wstring &src, std::string &dest);
	bool UnicodeToUtf8(std::wstring &src, std::string &dest);
	bool Utf8ToUnicode(std::string &src, std::wstring &dest);
}


#endif

