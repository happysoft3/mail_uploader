
#include "ccobject.h"

class CCObject::Core
{
private:
	CCObject *const m_thisObject;
	std::unique_ptr<std::promise<bool>> m_setExpired;

public:
	Core(std::unique_ptr<std::promise<bool>> pr, CCObject *thisObject)
		: m_thisObject(thisObject)
	{
		m_setExpired = std::move(pr);
	}
	~Core()
	{
		if (m_setExpired)
			m_setExpired->set_value(true);
	}

	CCObject *operator()()
	{
		return m_thisObject;
	}
};

CCObject::CCObject(CCObject *parent)
{
	if (parent)
		m_parent = parent->m_core;
	std::unique_ptr<std::promise<bool>> pr(new std::promise<bool>());
	m_allowDeleter = std::make_unique<std::future<bool>>(pr->get_future());
	m_core = std::make_unique<Core>(std::move(pr), this);
}

CCObject::~CCObject()
{
	m_core.reset();
	m_allowDeleter->wait();
}

//void CCObject::SetParent(CCObject *parent)
//{
//	if (!parent)
//		return;
//
//	m_parentTest = parent->m_core;
//}
