
#ifndef STRING_HELPER_H__
#define STRING_HELPER_H__

#include <string>
#include <stdexcept>
#include <memory>
#include <algorithm>

namespace _StringHelper
{
	template <class Container>
	inline auto toArray(Container &c)->decltype(&c[0])
	{
		return &c[0];
	}

	template <class Container>
	inline auto toArray(const Container &cc)
		->decltype(const_cast<typename Container::value_type *>(&cc[0]))
	{
		return const_cast<typename Container::value_type *>(cc.data());
	}

	template <class CharT>
	inline CharT *deleteStringConst(const std::basic_string<CharT> &cc)
	{
		return const_cast<CharT *>(cc.data());
	}

	template <class CharT>
	std::basic_string<CharT> AdjectCharT(const std::basic_string<CharT> &src, CharT tLeft, CharT tRight)
	{
		size_t srcLength = src.size();
		std::basic_string<CharT> tResult(srcLength + 2, 0);

		tResult[0] = tLeft;
		tResult[++srcLength] = tRight;
		std::copy(src.cbegin(), src.cend(), ++tResult.begin());

		return tResult;
	}

	template <class CharT>
	inline std::basic_string<CharT> AdjectCharT(const std::basic_string<CharT> &src, CharT tBoth)
	{
		return AdjectCharT(src, tBoth, tBoth);
	}

	namespace helperPrivate
	{
		template <class T>
		inline T strTransferT(T const &t)
		{
			return t;
		}

		template <class CharT>
		inline const CharT *strTransferT(const std::basic_string<CharT> &str)
		{
			return str.data();
		}

		template <class CharT>
		struct formatWriter;

		template <>
		struct formatWriter<char>
		{
			template <class... Args>
			static int Write(char *pDest, int length, const std::string &fmt, Args&&... args)
			{
				return std::snprintf(pDest, length, fmt.c_str(), std::forward<Args>(args)...);
			}
		};

		template <>
		struct formatWriter<wchar_t>
		{
			template <class... Args>
			static int Write(wchar_t *pDest, int length, const std::wstring &fmt, Args&&... args)
			{
				return _snwprintf(pDest, length, fmt.c_str(), std::forward<Args>(args)...);
			}
		};

		template <class CharT, typename... Args>
		std::basic_string<CharT> stringFormatPrivate(const std::basic_string<CharT> &format, Args ...args)
		{
			int length = formatWriter<CharT>::Write(nullptr, 0, format, std::forward<Args>(args)...) + 1;

			if (length <= 0)
				throw std::runtime_error("error - the length can't be 0 or minus!!");

			std::unique_ptr<CharT[]> pBuffer = std::unique_ptr<CharT[]>(new CharT[length]);

			formatWriter<CharT>::Write(pBuffer.get(), length, format, std::forward<Args>(args)...);
			return pBuffer.get();
		}
	}

	template <class CharT, class... Args>
	std::basic_string<CharT> stringFormat(const std::basic_string<CharT> &format, Args ...args)
	{
		return helperPrivate::stringFormatPrivate(format, helperPrivate::strTransferT(args)...);
	}

	template <class Ty, class... Args>
	std::basic_string<Ty> stringFormat(const Ty *format, Args ...args)
	{
		return helperPrivate::stringFormatPrivate(std::basic_string<Ty>(format), helperPrivate::strTransferT(args)...);
	}

	template <typename CharT>
	inline void stringLeftTrim(std::basic_string<CharT> &target)
	{
		target.erase(target.begin(), std::find_if(target.begin(), target.end(), [](const CharT &c) { return !isspace(c); }));
	}

	template <typename CharT>
	inline void stringRightTrim(std::basic_string<CharT> &target)
	{
		target.erase(std::find_if(target.rbegin(), target.rend(), [](const CharT &c) { return !isspace(c); }).base(), target.end());
	}

	template <typename CharT>
	void stringTrim(std::basic_string<CharT> &target)
	{
		stringLeftTrim(target);
		stringRightTrim(target);
	}
};

#endif
