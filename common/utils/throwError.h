
#ifndef THROW_ERROR_H__
#define THROW_ERROR_H__

#include <sstream>

class ThrowError
{
private:
	std::stringstream m_stream;

public:
	ThrowError();
	~ThrowError();

	template <class T>
	ThrowError &operator<<(const T &s)
	{
		m_stream << s;
		return *this;
	}
};

#endif
