
#include "taskQueue.h"
#include "taskUnit.h"

TaskQueue::TaskQueue()
	: CCObject()
{ }

TaskQueue::~TaskQueue()
{ }

void TaskQueue::Push(std::unique_ptr<TaskUnit> task)
{
	std::lock_guard<std::mutex> guard(m_lock);

	m_queue.push_back(std::move(task));
}

std::unique_ptr<TaskUnit> TaskQueue::Pop()
{
	std::unique_ptr<TaskUnit> task;
	{
		std::lock_guard<std::mutex> guard(m_lock);

		if (m_queue.empty())
			return {};

		task = std::move(m_queue.front());
		m_queue.pop_front();
	}
	return task;
}


