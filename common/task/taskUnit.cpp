
#include "taskunit.h"
#include <atomic>
#include <sstream>

class AtomicIndex
{
	static constexpr int index_limit = 131072;
private:
	std::atomic<int> m_first;
	std::atomic<int> m_secondary;
	std::atomic<int> m_third;

public:
	AtomicIndex()
		: m_first(0), m_secondary(0), m_third(0)
	{ }
	
	std::string Pull()
	{
		if (++m_first >= index_limit)
		{
			m_first = 0;
			if (++m_secondary >= index_limit)
			{
				m_secondary = 0;
				if (++m_third >= index_limit)
					m_third = 0;
			}
		}
		std::string ret;
		std::stringstream ss(ret);

		ss << m_first << ':' << m_secondary << ':' << m_third;
		ss >> ret;
		return ret;
	}
};

static AtomicIndex s_indexGen;

TaskUnit::TaskUnit(std::weak_ptr<TaskUnit::Controller> controller)
	: TaskObject()
{
	m_index = s_indexGen.Pull();
	m_taskControl = controller;
}

TaskUnit::~TaskUnit()
{ }

