
#include "taskObject.h"

TaskObject::TaskObject(CCObject *parent)
	: CCObject(parent)
{ }

TaskObject::~TaskObject()
{ }

void TaskObject::PutError(std::exception &failure)
{
	m_logicFailure = std::make_unique<std::exception>(failure);
}

std::unique_ptr<std::exception> TaskObject::ReleaseError()
{
	return m_logicFailure ? std::move(m_logicFailure) : nullptr;
}

std::unique_ptr<std::exception> TaskObject::GetError() const
{
	if (!m_logicFailure)
		return {};

	return std::make_unique<std::exception>(*m_logicFailure);
}
