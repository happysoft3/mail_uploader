
#ifndef TASK_UNIT_H__
#define TASK_UNIT_H__

#include "taskObject.h"
#include <string>

class TaskResult;

class TaskUnit : public TaskObject
{
public:
	class Controller
	{
	public:
		Controller(){ }
		virtual ~Controller(){ }
	};
private:
	std::string m_index;
	std::weak_ptr<Controller> m_taskControl;

public:
	TaskUnit(std::weak_ptr<Controller> controller = {});
	~TaskUnit() override;

protected:
	bool isExpired() const
	{
		return m_taskControl.expired();
	}

public:
	virtual void Run() = 0;
	virtual std::unique_ptr<TaskResult> GetResult() = 0;
	const char *const Index() const
	{
		return m_index.c_str();
	}
};

#endif

