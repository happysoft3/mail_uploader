
#include "pocoNetClientObject.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTMLForm.h"
#include "../include/incFilesystem.h"
#include "json/json.h"

#pragma comment (lib,"ws2_32.lib")
#pragma comment(lib, "Iphlpapi.lib")
#pragma comment (lib, "crypt32")

struct PocoNetClientObject::PocoImpl
{
	std::unique_ptr<Poco::URI> m_servUri;
	std::unique_ptr<Poco::Net::HTTPClientSession> m_session;
	Poco::Net::Context::Ptr m_contextPtr;
	std::string m_data;
	std::weak_ptr<Poco::Net::HTTPRequest> m_request;
};

PocoNetClientObject::PocoNetClientObject()
{
	m_core = std::make_unique<PocoImpl>();
}

PocoNetClientObject::PocoNetClientObject(PocoNetClientObject &other)
{	//세션 넘기기
	m_core = std::make_unique<PocoImpl>();

	m_core->m_session = (other.m_core->m_session) ? std::move(other.m_core->m_session) : nullptr;
	m_core->m_contextPtr = (other.m_core->m_contextPtr) ? std::move(other.m_core->m_contextPtr) : nullptr;
	m_core->m_servUri = (other.m_core->m_servUri) ? std::move(other.m_core->m_servUri) : nullptr;
}

PocoNetClientObject::~PocoNetClientObject()
{ }

void PocoNetClientObject::makeSession(Poco::URI &uri)
{
	using namespace Poco::Net;
	m_core->m_contextPtr = new Context(
		Poco::Net::Context::CLIENT_USE, "", "", "", Context::VERIFY_NONE, 9, false, "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
	std::unique_ptr<HTTPClientSession> session;

	if (uri.getScheme() == "https")
		session = std::unique_ptr<HTTPClientSession>(new HTTPSClientSession(uri.getHost(), uri.getPort(), m_core->m_contextPtr));
	else
		session = std::make_unique<HTTPClientSession>(uri.getHost(), uri.getPort());

	session->setKeepAlive(true);
	m_core->m_session = std::move(session);
}

void PocoNetClientObject::onPreSendRequest(Poco::Net::HTTPRequest &req)
{
	req.setContentType("text/plain");
	req.setContentLength(m_core->m_data.size());

	m_core->m_data = __FUNCDNAME__;
}

void PocoNetClientObject::onSendRequest(std::ostream &os)
{
	os << m_core->m_data;
}

void PocoNetClientObject::onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw)
{
	//Get status code
	int statusCode = static_cast<int>(resp.getStatus());

	//Get status
	std::string status = resp.getReason();
}

void PocoNetClientObject::setRequestContentLength(uint64_t length)
{
	auto req = m_core->m_request.lock();

	if (req)
		req->setContentLength(length);
}

void PocoNetClientObject::sendRequest(const std::string &method, const std::string &uri, const std::string &version)
{
	using namespace Poco::Net;
	auto request = std::make_shared<HTTPRequest>(method, uri, version);

	m_core->m_request = request;
	request->setKeepAlive(true);
	onPreSendRequest(*request);

	std::ostream& out = m_core->m_session->sendRequest(*request);
	onSendRequest(out);
}

void PocoNetClientObject::sendRequest(const std::string &method, const std::string &version)
{
	settingRequestUri(*m_core->m_servUri);
	std::string uri(m_core->m_servUri->getPathAndQuery());

	if (uri.empty())
		uri = '/';
	sendRequest(method, uri, version);
}

void PocoNetClientObject::receiveResponse()
{
	using namespace Poco::Net;
	auto resp = std::make_unique<HTTPResponse>();
	std::istream& rs = m_core->m_session->receiveResponse(*resp);
	std::string raw(std::istreambuf_iterator<char>(rs), {});

	onReceiveResponse(*resp, raw);
}

void PocoNetClientObject::runClient()
{
	using namespace Poco::Net;
	sendRequest(HTTPRequest::HTTP_GET, "/", HTTPMessage::HTTP_1_1);
	receiveResponse();
}

void PocoNetClientObject::PutServerUrl(const std::string &url)
{
	auto servUri = std::make_unique<Poco::URI>(url);

	makeSession(*servUri);
	m_core->m_servUri = std::move(servUri);
}

bool PocoNetClientObject::Run()
{
	try
	{
		if (!m_core->m_session)
			throw std::exception("no session");

		runClient();
		return true;
	}
	catch (std::exception &ex)
	{
		m_error = std::make_unique<std::exception>(ex);
	}
	return false;
}
