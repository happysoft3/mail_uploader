
#ifndef POCO_NET_CLIENT_OBJECT_H__
#define POCO_NET_CLIENT_OBJECT_H__

#include <string>
#include <memory>

namespace Poco
{
	class URI;
	namespace Net
	{
		class HTTPRequest;
		class HTTPResponse;
	}
}

class PocoNetClientObject
{
	struct PocoImpl;

private:
	std::unique_ptr<PocoImpl> m_core;
	std::unique_ptr<std::exception> m_error;

public:
	PocoNetClientObject();
	PocoNetClientObject(PocoNetClientObject &other);
	virtual ~PocoNetClientObject();

private:
	void makeSession(Poco::URI &uri);
	virtual void onPreSendRequest(Poco::Net::HTTPRequest &req);
	virtual void onSendRequest(std::ostream &os);
	virtual void onReceiveResponse(Poco::Net::HTTPResponse &resp, const std::string &raw);

protected:
	void setRequestContentLength(uint64_t length);
	void sendRequest(const std::string &method, const std::string &uri, const std::string &version);
	virtual void settingRequestUri(Poco::URI &uri) { }
	void sendRequest(const std::string &method, const std::string &version);
	void receiveResponse();

private:
	virtual void runClient();

public:
	const char *const DetailError() const
	{
		return m_error ? m_error->what() : "";
	}
	void PutServerUrl(const std::string &url);
	bool Run();
};

#endif

