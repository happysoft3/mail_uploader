
#include "ccobjectTask.h"
#include "task/taskResult.h"

CCObjectTask::CCObjectTask(std::weak_ptr<CCObject::Core> target, std::function<void()> &&slotInvoke)
	: TaskUnit()
{
	m_target = target;
	m_invoke = std::forward<std::function<void()>>(slotInvoke);
}

CCObjectTask::~CCObjectTask()
{ }

void CCObjectTask::Run()
{
	m_result = std::make_unique<TaskResult>();
	auto real = m_target.lock();

	if (real)
		m_invoke();
	else
		m_result->EmplaceError<std::exception>("the object is not exist");
}

std::unique_ptr<TaskResult> CCObjectTask::GetResult()
{
	return m_result ? std::move(m_result) : nullptr;
}
