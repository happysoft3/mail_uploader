
#ifndef CCOBJECT_RESULT_H__
#define CCOBJECT_RESULT_H__

#include "task/taskResult.h"

class CCObjectResult : public TaskResult
{
private:
	int m_count;
	int m_errorCount;

public:
	CCObjectResult();
	~CCObjectResult() override;
};

#endif

