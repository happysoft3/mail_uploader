
#ifndef CCOBJECT_THREAD_H__
#define CCOBJECT_THREAD_H__

#include "task/taskThread.h"
#include "ccobjectTask.h"
#include <atomic>

class CCObjectThread : public TaskThread
{
private:
	std::shared_ptr<TaskQueue> m_queue;
	std::atomic<bool> m_threadAlive;

public:
	CCObjectThread();
	~CCObjectThread() override;

private:
	void doWork();

public:
	void PushQueue(CCObject *pObject, std::function<void()> &&fn);

	template <class... SignalArgs, class... Args>
	void PutTask(EventSignal<SignalArgs...> *pSignal, Args&&... args)
	{
		if (!pSignal)
			return;

		CCObject *object = dynamic_cast<CCObject *>(pSignal);

		if (!object)
			return;

		auto ref = loadCore(object);
		auto real = ref.lock();

		if (real)
		{
			pushQueue(std::make_unique<CCObjectTask>(ref, [args..., pSignal](){
				pSignal->Emit(std::forward<Args>(args)...);
			}));
		}
	}

private:
	void afterExec(TaskResultHash &resultMap) noexcept override;
};

#endif

