
#include "ccobjectThread.h"
#include "task/taskQueue.h"
#include "task/taskResultHash.h"
#include "task/taskResult.h"

CCObjectThread::CCObjectThread()
	: TaskThread()
{
	m_queue = std::make_unique<TaskQueue>();
	m_threadAlive = false;
}

CCObjectThread::~CCObjectThread()
{
	Wait();
}

void CCObjectThread::doWork()
{
	Perform(m_queue);
}

void CCObjectThread::PushQueue(CCObject *pObject, std::function<void()> &&fn)
{
	auto weak = loadCore(pObject);
	auto core = weak.lock();

	if (!core)
		return;

	m_queue->Push(std::make_unique<CCObjectTask>(weak, std::move(fn)));
	if (!m_threadAlive)
	{
		m_threadAlive = true;
		auto resMap = GetResult();

		if (resMap)
		{
			for (;;)
			{
				auto res = resMap->Pop();

				if (!res)
					break;
				//아무 처리 안합니다...
			}
		}
		doWork();
	}
}

void CCObjectThread::afterExec(TaskResultHash &resultMap) noexcept
{
	m_threadAlive = false;
}
