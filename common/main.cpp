﻿// common.cpp : 이 파일에는 'main' 함수가 포함됩니다. 거기서 프로그램 실행이 시작되고 종료됩니다.
//

#include "ccobject/ccobject.h"
#include <iostream>
#include <chrono>

#pragma comment(lib, "winmm.lib")
#pragma comment(lib, "wldap32.lib")
#pragma comment(lib,"shlwapi.lib")

class TestObject : public CCObject
{
private:
	int m_any;

public:
	TestObject(int n = 0)
		: CCObject()
	{
		m_any = n;
	}
	~TestObject() override
	{
		std::cout << m_any << "deleted" << std::endl;
	}

	void Print() const
	{
		std::cout << m_any << std::endl;
	}
};

void Deffered(std::unique_ptr<TestObject> other)
{
	other->Print();
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	other->Print();
	std::this_thread::sleep_for(std::chrono::milliseconds(300));
	other->Print();
}

int main()
{
	std::thread worker;
    std::cout << "Hello World!\n";
	{
		std::shared_ptr<TestObject> any(new TestObject(1));
		auto other = std::make_unique<TestObject>(2);

		//other->SetParent(any.get());
		worker = std::thread([](std::unique_ptr<TestObject> ot) { Deffered(std::move(ot)); }, std::move(other));
		any->Print();
	}
	std::cout << "over scope\n";
	worker.join();
	return 0;
}

// 프로그램 실행: <Ctrl+F5> 또는 [디버그] > [디버깅하지 않고 시작] 메뉴
// 프로그램 디버그: <F5> 키 또는 [디버그] > [디버깅 시작] 메뉴

// 시작을 위한 팁: 
//   1. [솔루션 탐색기] 창을 사용하여 파일을 추가/관리합니다.
//   2. [팀 탐색기] 창을 사용하여 소스 제어에 연결합니다.
//   3. [출력] 창을 사용하여 빌드 출력 및 기타 메시지를 확인합니다.
//   4. [오류 목록] 창을 사용하여 오류를 봅니다.
//   5. [프로젝트] > [새 항목 추가]로 이동하여 새 코드 파일을 만들거나, [프로젝트] > [기존 항목 추가]로 이동하여 기존 코드 파일을 프로젝트에 추가합니다.
//   6. 나중에 이 프로젝트를 다시 열려면 [파일] > [열기] > [프로젝트]로 이동하고 .sln 파일을 선택합니다.
